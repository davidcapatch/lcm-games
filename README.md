## lcm-games

## Installation Instructions

This repo uses `pipenv` which is a python library for managing python virtual environments.

1.  git clone lcm-games
2.  cd lcm-games
3.  pipenv install

## Running Instructions

The `main.py` file the primary game script.

1.  Start at top of repo
2.  pipenv shell
3.  cd src
4.  python main.py

## Controls

The game will enter into the game select screen on run.

`ESCAPE`: Will exit the main script loop when on the game select screen
`ENTER/RETURN`: The main "do action" button, select/enter/fire/etc
`BACKSPACE`: Main back/exit button when inside of a game
`ARROW KEYS`: Basic left/right/up/down
`TAB`: Open game menu

# Projectile Gravity

`A`: Power up
`Z`: Power down
`S`: Cannon angel up
`X`: Cannon angel down


# Add font to system fonts
```bash
cp <font> `/usr/local/share/fonts`
```


# Launch game at startup
https://www.raspberrypi.org/forums/viewtopic.php?f=27&t=11256

This is what I do for a Python progrma; no need to edit a system file:

•	Create the folder home/pi/.config/lxsession/LXDE

•	In this folder put a file named autostart containing one line specifying the full path of the file to be executed:

e.g. python /home/pi/pipresents/pipresents.py <options>

•	Make the autostart file executable by using the properties menu in the File Manager to alter the permissions on the file.