#include "Rotary.h"
#include "include/constants.h"

#include <Adafruit_GFX.h>
#include <Adafruit_SPITFT_Macros.h>
#include <Adafruit_SPITFT.h>
#include <gfxfont.h>

#include <Wire.h>
#include <Adafruit_LEDBackpack.h>

#include <Bounce2.h>


// Debouncing
#define BUTTON_INTERVAL 25

// Define rotary encoders
Rotary r1 = Rotary(P3EINP2B1, P3EINP2B3);
// Define buttons
Bounce P2EINP1A_b = Bounce();
Bounce P2EINP1B_b = Bounce();
Bounce P2EINP2A_b = Bounce();
Bounce P2EINP2B_b = Bounce();
Bounce P2EINP3A_b = Bounce();
Bounce P2EINP3B_b = Bounce();
//
Bounce P2FINP1A_b = Bounce();
Bounce P2FINP1B_b = Bounce();
Bounce P2FINP1C_b = Bounce();
Bounce P2FINP2A_b = Bounce();
Bounce P2FINP2B_b = Bounce();
Bounce P2FINP2C_b = Bounce();
Bounce P2FINP3A_b = Bounce();
Bounce P2FINP3B_b = Bounce();
Bounce P2FINP3C_b = Bounce();

// Define interrupt functions
// NOTE: For some odd reason it matters where this
// Function gets defined. FOr instance if it is defined
// bellow the next section for the led matrixes it will
// not compile.
void check1()
{
    unsigned char result = r1.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1031,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1031,0");
    }
}

/***********************************************
* Count down using millis()
*
* Adafruit 7-segment, I2C, LED backpack display
*
* pin A5 -> C (on 7-segment)
* pin A4 -> D (on 7-segment)
*
************************************************/

Adafruit_7segment matrix1 = Adafruit_7segment();
Adafruit_7segment matrix2 = Adafruit_7segment();

unsigned long previousSecondMillis = 0UL;
long oneSecond = 1000UL; // milliseconds per second

#define startMinute1 99  //  Modify these defines to
#define startSecond1 59 // change the timer interval
int minutes1 = startMinute1;
int seconds1 = startSecond1;

#define startMinute2 4 //  Modify these defines to
#define startSecond2 59 // change the timer interval
int minutes2 = startMinute2;
int seconds2 = startSecond2;

void setup()
{

    matrix1.begin(0x70);
    matrix2.begin(0x71);

    //start serial connection
    Serial.begin(9600);

    // Attach interrupts to rotary encoders
    attachInterrupt(digitalPinToInterrupt(P3EINP2B1), check1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3EINP2B3), check1, CHANGE);

    // ROW 2
    // Digital pins 22-52
    P2EINP1A_b.attach(P2EINP1A, INPUT_PULLUP);
    P2EINP1A_b.interval(BUTTON_INTERVAL);
    P2EINP1B_b.attach(P2EINP1B, INPUT_PULLUP);
    P2EINP1B_b.interval(BUTTON_INTERVAL);
    P2EINP2A_b.attach(P2EINP2A, INPUT_PULLUP);
    P2EINP2A_b.interval(BUTTON_INTERVAL);
    P2EINP2B_b.attach(P2EINP2B, INPUT_PULLUP);
    P2EINP2B_b.interval(BUTTON_INTERVAL);
    P2EINP3A_b.attach(P2EINP3A, INPUT_PULLUP);
    P2EINP3A_b.interval(BUTTON_INTERVAL);
    P2EINP3B_b.attach(P2EINP3B, INPUT_PULLUP);
    P2EINP3B_b.interval(BUTTON_INTERVAL);
    //
    P2FINP1A_b.attach(P2FINP1A, INPUT_PULLUP);
    P2FINP1A_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED1A, OUTPUT);
    P2FINP1B_b.attach(P2FINP1B, INPUT_PULLUP);
    P2FINP1B_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED1B, OUTPUT);
    P2FINP1C_b.attach(P2FINP1C, INPUT_PULLUP);
    P2FINP1C_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED1C, OUTPUT);
    P2FINP2A_b.attach(P2FINP2A, INPUT_PULLUP);
    P2FINP2A_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED2A, OUTPUT);
    P2FINP2B_b.attach(P2FINP2B, INPUT_PULLUP);
    P2FINP2B_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED2B, OUTPUT);
    P2FINP2C_b.attach(P2FINP2C, INPUT_PULLUP);
    P2FINP2C_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED2C, OUTPUT);
    P2FINP3A_b.attach(P2FINP3A, INPUT_PULLUP);
    P2FINP3A_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED3A, OUTPUT);
    P2FINP3B_b.attach(P2FINP3B, INPUT_PULLUP);
    P2FINP3B_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED3B, OUTPUT);
    P2FINP3C_b.attach(P2FINP3C, INPUT_PULLUP);
    P2FINP3C_b.interval(BUTTON_INTERVAL);
    pinMode(P2FLED3C, OUTPUT);
}

void loop()
{

    // --------- Run this every second ------------------
    if (millis() - previousSecondMillis >= oneSecond)
    {

        matrix1.writeDigitNum(0, (minutes1 / 10));
        matrix1.writeDigitNum(1, (minutes1 % 10));
        matrix1.writeDigitNum(3, (seconds1 / 10));
        matrix1.writeDigitNum(4, (seconds1 % 10));
        matrix1.writeDisplay();

        matrix2.writeDigitNum(0, (minutes2 / 10));
        matrix2.writeDigitNum(1, (minutes2 % 10));
        matrix2.writeDigitNum(3, (seconds2 / 10));
        matrix2.writeDigitNum(4, (seconds2 % 10));
        matrix2.writeDisplay();

        if (seconds1-- == 0)
        {
            if (minutes1 == 0)
            {
                minutes1 = startMinute1;
                seconds1 = startSecond1;
                delay(1000);
            }
            else
            {
                minutes1 -= 1;
                seconds1 = 59;
            }
        }
        if (seconds2-- == 0)
        {
            if (minutes2 == 0)
            {
                minutes2 = startMinute2;
                seconds2 = startSecond2;
                delay(1000);
            }
            else
            {
                minutes2 -= 1;
                seconds2 = 59;
            }
        }

        previousSecondMillis += oneSecond;
    }

    // ------------------------------------------------
    // Read the pushbutton value into a variable
    // ------------------------------------------------

    // ROW 2
    P2EINP1A_b.update();
    P2EINP1B_b.update();
    P2EINP2A_b.update();
    P2EINP2B_b.update();
    P2EINP3A_b.update();
    P2EINP3B_b.update();
    //
    P2FINP1A_b.update();
    P2FINP1B_b.update();
    P2FINP1C_b.update();
    P2FINP2A_b.update();
    P2FINP2B_b.update();
    P2FINP2C_b.update();
    P2FINP3A_b.update();
    P2FINP3B_b.update();
    P2FINP3C_b.update();

    // ROW 2
    // Digital pins 22-52
    if (P2EINP1A_b.fell())
    {
        Serial.println("1053,0");
    }
    else if (P2EINP1A_b.rose())
    {
        Serial.println("1053,1");
    };
    if (P2EINP1B_b.fell())
    {
        Serial.println("1054,0");
    }
    else if (P2EINP1B_b.rose())
    {
        Serial.println("1054,1");
    };
    if (P2EINP2A_b.fell())
    {
        Serial.println("1055,0");
    }
    else if (P2EINP2A_b.rose())
    {
        Serial.println("1055,1");
    };
    if (P2EINP2B_b.fell())
    {
        Serial.println("1056,0");
    }
    else if (P2EINP2B_b.rose())
    {
        Serial.println("1056,1");
    };
    if (P2EINP3A_b.fell())
    {
        Serial.println("1057,0");
    }
    else if (P2EINP3A_b.rose())
    {
        Serial.println("1057,1");
    };
    if (P2EINP3B_b.fell())
    {
        Serial.println("1058,0");
    }
    else if (P2EINP3B_b.rose())
    {
        Serial.println("1058,1");
    };
    //
    if (P2FINP1A_b.fell())
    {
        Serial.println("1059,1");
        P2FLED1A_state = 1;
    }
    else if (P2FINP1A_b.rose())
    {
        Serial.println("1059,0");
        P2FLED1A_state = 0;
    };
    if (P2FINP1B_b.fell())
    {
        Serial.println("1060,1");
        P2FLED1B_state = 1;
    }
    else if (P2FINP1B_b.rose())
    {
        Serial.println("1060,0");
        P2FLED1B_state = 0;
    };
    if (P2FINP1C_b.fell())
    {
        Serial.println("1061,1");
        P2FLED1C_state = 1;
    }
    else if (P2FINP1C_b.rose())
    {
        Serial.println("1061,0");
        P2FLED1C_state = 0;
    };
    if (P2FINP2A_b.fell())
    {
        Serial.println("1062,1");
        P2FLED2A_state = 1;
    }
    else if (P2FINP2A_b.rose())
    {
        Serial.println("1062,0");
        P2FLED2A_state = 0;
    };
    if (P2FINP2B_b.fell())
    {
        Serial.println("1063,1");
        P2FLED2B_state = 1;
    }
    else if (P2FINP2B_b.rose())
    {
        Serial.println("1063,0");
        P2FLED2B_state = 0;
    };
    if (P2FINP2C_b.fell())
    {
        Serial.println("1064,1");
        P2FLED2C_state = 1;
    }
    else if (P2FINP2C_b.rose())
    {
        Serial.println("1064,0");
        P2FLED2C_state = 0;
    };
    if (P2FINP3A_b.fell())
    {
        Serial.println("1065,1");
        P2FLED3A_state = 1;
    }
    else if (P2FINP3A_b.rose())
    {
        Serial.println("1065,0");
        P2FLED3A_state = 0;
    };
    if (P2FINP3B_b.fell())
    {
        Serial.println("1066,1");
        P2FLED3B_state = 1;
    }
    else if (P2FINP3B_b.rose())
    {
        Serial.println("1066,0");
        P2FLED3B_state = 0;
    };
    if (P2FINP3C_b.fell())
    {
        Serial.println("1067,1");
        P2FLED3C_state = 1;
    }
    else if (P2FINP3C_b.rose())
    {
        Serial.println("1067,0");
        P2FLED3C_state = 0;
    };

    // ------------------------------------------------
    // Writing to LEDS
    // ------------------------------------------------
    digitalWrite(P2FLED1A, 1);
    digitalWrite(P2FLED1B, 1);
    digitalWrite(P2FLED1C, 1);
    digitalWrite(P2FLED2A, 1);
    digitalWrite(P2FLED2B, 1);
    digitalWrite(P2FLED2C, 1);
    digitalWrite(P2FLED3A, 1);
    digitalWrite(P2FLED3B, 1);
    digitalWrite(P2FLED3C, 1);
}
