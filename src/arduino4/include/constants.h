//----------------------------
// PANEL ROW 3
//----------------------------
const int P3EINP2B1 = 2;
const int P3EINP2B3 = 3;

//----------------------------
// PANEL ROW 2
//----------------------------
// MEGA 2560 Digital Pins 22-53
const int P2EINP1A = 22;
const int P2EINP1B = 23;
const int P2EINP2A = 24;
const int P2EINP2B = 25;
const int P2EINP3A = 26;
const int P2EINP3B = 27;
//
const int P2FINP1A = 28;
const int P2FLED1A = 29;
const int P2FINP1B = 30;
const int P2FLED1B = 31;
const int P2FINP1C = 32;
const int P2FLED1C = 33;
const int P2FINP2A = 34;
const int P2FLED2A = 35;
const int P2FINP2B = 36;
const int P2FLED2B = 37;
const int P2FINP2C = 38;
const int P2FLED2C = 39;
const int P2FINP3A = 40;
const int P2FLED3A = 41;
const int P2FINP3B = 42;
const int P2FLED3B = 43;
const int P2FINP3C = 44;
const int P2FLED3C = 45;

//----------------------------
// PANEL ROW 2
// Storage for button LEDs
// Digital Pins 22-53
//----------------------------

int P2FLED1A_state = 0;
int P2FLED1B_state = 0;
int P2FLED1C_state = 0;
int P2FLED2A_state = 0;
int P2FLED2B_state = 0;
int P2FLED2C_state = 0;
int P2FLED3A_state = 0;
int P2FLED3B_state = 0;
int P2FLED3C_state = 0;
