import os
import math
import pygame
from constants import PHYSICAL_SCREEN
from constants import ALIGN_LEFT, ALIGN_CENTER_H, ALIGN_RIGHT, ALIGN_TOP, ALIGN_CENTER_V, ALIGN_BOTTOM

class Text(pygame.sprite.DirtySprite):
    ''' Master Text Sprite '''

    def __init__(self, pos, color, typeface, text, align_h=ALIGN_LEFT, align_v=ALIGN_TOP):

        super(Text, self).__init__()

        self.color = color
        self.typeface = typeface
        self.text_string = text
        self.text = typeface.render(str(text), 1, self.color)
        self.align_h = align_h
        self.align_v = align_v
        self.master_position = pos
        self.position = self.get_position()
        self.dirty = 1
        self.image = self.text
        self.rect = pygame.Rect(self.position, (self.text.get_rect().width, self.text.get_rect().height))

    def set_dirty(self):
        ''' Set text to update '''
        self.dirty = 1

    def get_position(self):
        ''' get new position of text '''
        x = 0
        y = 0
        if self.align_h == ALIGN_LEFT:
            x = self.master_position[0]
        elif self.align_h == ALIGN_CENTER_H:
            x = self.master_position[0] - self.text.get_rect().width / 2
        elif self.align_h == ALIGN_RIGHT:
            x = self.master_position[0] - self.text.get_rect().width

        if self.align_v == ALIGN_TOP:
            y = self.master_position[1]
        elif self.align_v == ALIGN_CENTER_V:
            y = self.master_position[1] - self.text.get_rect().height / 2
        elif self.align_v == ALIGN_BOTTOM:
            y = self.master_position[1] - self.text.get_rect().height

        return (x, y)

    def update(self, text):
        ''' Update text '''
        self.text = self.typeface.render(str(text), 1, self.color)
        self.image = self.text
        self.position = self.get_position()
        self.rect = pygame.Rect(self.position, (self.text.get_rect().width, self.text.get_rect().height))
        self.set_dirty()

    def update_color(self, color):
        self.color = color
        self.text = self.typeface.render(self.text_string, 1, color)
        self.image = self.text
        self.position = self.get_position()
        self.rect = pygame.Rect(self.position, (self.text.get_rect().width, self.text.get_rect().height))
        self.set_dirty()


class Asset(pygame.sprite.DirtySprite):
    ''' Master Asset Sprite '''

    def __init__(self, pos, image, rect=None, create_dirty=True, align_h=ALIGN_LEFT, align_v=ALIGN_TOP):

        super(Asset, self).__init__()

        self.align_h = align_h
        self.align_v = align_v
        self.dirty = 0
        if create_dirty:
            self.dirty = 1
        self.image = image
        if rect:
            self.image = self.image.subsurface(rect).copy()

        self.position = self.get_position(pos)

        self.rect = pygame.Rect(self.position, (self.image.get_rect().width, self.image.get_rect().height))

    def set_dirty(self):
        ''' Set image to update '''
        self.dirty = 1

    def get_position(self, pos):
        ''' get new position of image '''
        x = 0
        y = 0
        if self.align_h == ALIGN_LEFT:
            x = pos[0]
        elif self.align_h == ALIGN_CENTER_H:
            x = pos[0] - self.image.get_rect().width / 2
        elif self.align_h == ALIGN_RIGHT:
            x = pos[0] - self.image.get_rect().width

        if self.align_v == ALIGN_TOP:
            y = pos[1]
        elif self.align_v == ALIGN_CENTER_V:
            y = pos[1] - self.image.get_rect().height / 2
        elif self.align_v == ALIGN_BOTTOM:
            y = pos[1] - self.image.get_rect().height

        return (x, y)

    def update(self, pos=None, image=None):
        ''' Update image '''
        if pos:
            self.position = self.get_position(pos)
        else:
            self.position = self.get_position(self.position)
        if image:
            self.image = image

        self.rect = pygame.Rect(self.position, (self.image.get_rect().width, self.image.get_rect().height))
