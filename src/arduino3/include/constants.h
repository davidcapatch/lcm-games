//----------------------------
// PANEL ROW 3
//----------------------------
const int P3EINP1A1 = 2;
const int P3EINP1A3 = 3;
const int P3EINP1B1 = 18;
const int P3EINP1B3 = 19;
const int P3EINP2A1 = 20;
const int P3EINP2A3 = 21;

//----------------------------
// PANEL ROW 2
//----------------------------
// MEGA 2560 Digital Pins 22-53
const int P2AINP1A = 22;
const int P2ALED1A = 23;
const int P2AINP1B = 24;
const int P2ALED1B = 25;
const int P2AINP1C = 26;
const int P2ALED1C = 27;
const int P2AINP2A = 28;
const int P2ALED2A = 29;
const int P2AINP2B = 30;
const int P2ALED2B = 31;
const int P2AINP2C = 32;
const int P2ALED2C = 33;
const int P2AINP3A = 34;
const int P2ALED3A = 35;
const int P2AINP3B = 36;
const int P2ALED3B = 37;
const int P2AINP3C = 38;
const int P2ALED3C = 39;
//
const int P2BINP1A = 40;
const int P2BINP1B = 41;
const int P2BINP2A = 42;
const int P2BINP2B = 43;
const int P2BINP3A = 44;
const int P2BINP3B = 45;
//
const int P2CINP1A = 46;
const int P2CLED1A = 47;
const int P2CINP1B = 48;
const int P2CLED1B = 49;
const int P2CINP2A = 50;
const int P2CLED2A = 51;
const int P2CINP2B = 52;
const int P2CLED2B = 53;

//----------------------------
// PANEL ROW 2
// Storage for button LEDs
// Digital Pins 22-53
//----------------------------

int P2ALED1A_state = 0;
int P2ALED1B_state = 0;
int P2ALED1C_state = 0;
int P2ALED2A_state = 0;
int P2ALED2B_state = 0;
int P2ALED2C_state = 0;
int P2ALED3A_state = 0;
int P2ALED3B_state = 0;
int P2ALED3C_state = 0;
//
int P2CLED1A_state = 0;
int P2CLED1B_state = 0;
int P2CLED2A_state = 0;
int P2CLED2B_state = 0;
