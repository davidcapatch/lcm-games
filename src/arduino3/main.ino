#include <Bounce2.h>

#include "Rotary.h"
#include "include/constants.h"

#define BUTTON_INTERVAL 25


// Define rotary encoders
Rotary r1 = Rotary(P3EINP1A1, P3EINP1A3);
Rotary r2 = Rotary(P3EINP1B1, P3EINP1B3);
Rotary r3 = Rotary(P3EINP2A1, P3EINP2A3);
// Define buttons
Bounce P2AINP1A_b = Bounce();
Bounce P2AINP1B_b = Bounce();
Bounce P2AINP1C_b = Bounce();
Bounce P2AINP2A_b = Bounce();
Bounce P2AINP2B_b = Bounce();
Bounce P2AINP2C_b = Bounce();
Bounce P2AINP3A_b = Bounce();
Bounce P2AINP3B_b = Bounce();
Bounce P2AINP3C_b = Bounce();
//
Bounce P2BINP1A_b = Bounce();
Bounce P2BINP1B_b = Bounce();
Bounce P2BINP2A_b = Bounce();
Bounce P2BINP2B_b = Bounce();
Bounce P2BINP3A_b = Bounce();
Bounce P2BINP3B_b = Bounce();
//
Bounce P2CINP1A_b = Bounce();
Bounce P2CINP1B_b = Bounce();
Bounce P2CINP2A_b = Bounce();
Bounce P2CINP2B_b = Bounce();

// Define interrupt functions
void check1()
{
    unsigned char result = r1.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1025,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1025,0");
    }
}

void check2()
{
    unsigned char result = r2.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1027,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1027,0");
    }
}

void check3()
{
    unsigned char result = r3.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1029,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1029,0");
    }
}

void setup()
{
    //start serial connection
    Serial.begin(9600);

    // Attach interrupts to rotary encoders
    attachInterrupt(digitalPinToInterrupt(P3EINP1A1), check1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3EINP1A3), check1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3EINP1B1), check2, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3EINP1B3), check2, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3EINP2A1), check3, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3EINP2A3), check3, CHANGE);
    
    // ROW 2
    // Digital pins 22-52
    // pinMode(P1FLEDS, OUTPUT);
    // BLANK
    P2AINP1A_b.attach(P2AINP1A, INPUT_PULLUP);
    P2AINP1A_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED1A, OUTPUT);
    P2AINP1B_b.attach(P2AINP1B, INPUT_PULLUP);
    P2AINP1B_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED1B, OUTPUT);
    P2AINP1C_b.attach(P2AINP1C, INPUT_PULLUP);
    P2AINP1C_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED1C, OUTPUT);
    P2AINP2A_b.attach(P2AINP2A, INPUT_PULLUP);
    P2AINP2A_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED2A, OUTPUT);
    P2AINP2B_b.attach(P2AINP2B, INPUT_PULLUP);
    P2AINP2B_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED2B, OUTPUT);
    P2AINP2C_b.attach(P2AINP2C, INPUT_PULLUP);
    P2AINP2C_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED2C, OUTPUT);
    P2AINP3A_b.attach(P2AINP3A, INPUT_PULLUP);
    P2AINP3A_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED3A, OUTPUT);
    P2AINP3B_b.attach(P2AINP3B, INPUT_PULLUP);
    P2AINP3B_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED3B, OUTPUT);
    P2AINP3C_b.attach(P2AINP3C, INPUT_PULLUP);
    P2AINP3C_b.interval(BUTTON_INTERVAL);
    pinMode(P2ALED3C, OUTPUT);
    //

    P2BINP1A_b.attach(P2BINP1A, INPUT_PULLUP);
    P2BINP1A_b.interval(BUTTON_INTERVAL);
    P2BINP1B_b.attach(P2BINP1B, INPUT_PULLUP);
    P2BINP1B_b.interval(BUTTON_INTERVAL);
    P2BINP2A_b.attach(P2BINP2A, INPUT_PULLUP);
    P2BINP2A_b.interval(BUTTON_INTERVAL);
    P2BINP2B_b.attach(P2BINP2B, INPUT_PULLUP);
    P2BINP2B_b.interval(BUTTON_INTERVAL);
    P2BINP3A_b.attach(P2BINP3A, INPUT_PULLUP);
    P2BINP3A_b.interval(BUTTON_INTERVAL);
    P2BINP3B_b.attach(P2BINP3B, INPUT_PULLUP);
    P2BINP3B_b.interval(BUTTON_INTERVAL);
    //
    P2CINP1A_b.attach(P2CINP1A, INPUT_PULLUP);
    P2CINP1A_b.interval(BUTTON_INTERVAL);
    pinMode(P2CLED1A, OUTPUT);
    P2CINP1B_b.attach(P2CINP1B, INPUT_PULLUP);
    P2CINP1B_b.interval(BUTTON_INTERVAL);
    pinMode(P2CLED1B, OUTPUT);
    P2CINP2A_b.attach(P2CINP2A, INPUT_PULLUP);
    P2CINP2A_b.interval(BUTTON_INTERVAL);
    pinMode(P2CLED2A, OUTPUT);
    P2CINP2B_b.attach(P2CINP2B, INPUT_PULLUP);
    P2CINP2B_b.interval(BUTTON_INTERVAL);
    pinMode(P2CLED2B, OUTPUT);

}

void loop()
{
    // ------------------------------------------------
    // Read the pushbutton value into a variable
    // ------------------------------------------------

    // ROW 2
    P2AINP1A_b.update();
    P2AINP1B_b.update();
    P2AINP1C_b.update();
    P2AINP2A_b.update();
    P2AINP2B_b.update();
    P2AINP2C_b.update();
    P2AINP3A_b.update();
    P2AINP3B_b.update();
    P2AINP3C_b.update();
    //
    P2BINP1A_b.update();
    P2BINP1B_b.update();
    P2BINP2A_b.update();
    P2BINP2B_b.update();
    P2BINP3A_b.update();
    P2BINP3B_b.update();
    //
    P2CINP1A_b.update();
    P2CINP1B_b.update();
    P2CINP2A_b.update();
    P2CINP2B_b.update();


    // ROW 2
    // Digital pins 22-52
    if (P2AINP1A_b.fell())
    {
        Serial.println("1034,1");
        P2ALED1A_state = 1;
    }
    else if (P2AINP1A_b.rose())
    {
        Serial.println("1034,0");
        P2ALED1A_state = 0;
    };
    if (P2AINP1B_b.fell())
    {
        Serial.println("1035,1");
        P2ALED1B_state = 1;
    }
    else if (P2AINP1B_b.rose())
    {
        Serial.println("1035,0");
        P2ALED1B_state = 0;
    };
    if (P2AINP1C_b.fell())
    {
        Serial.println("1036,1");
        P2ALED1C_state = 1;
    }
    else if (P2AINP1C_b.rose())
    {
        Serial.println("1036,0");
        P2ALED1C_state = 0;
    };
    if (P2AINP2A_b.fell())
    {
        Serial.println("1037,1");
        P2ALED2A_state = 1;
    }
    else if (P2AINP2A_b.rose())
    {
        Serial.println("1037,0");
        P2ALED2A_state = 0;
    };
    if (P2AINP2B_b.fell())
    {
        Serial.println("1038,1");
        P2ALED2B_state = 1;
    }
    else if (P2AINP2B_b.rose())
    {
        Serial.println("1038,0");
        P2ALED2B_state = 0;
    };
    if (P2AINP2C_b.fell())
    {
        Serial.println("1039,1");
        P2ALED2C_state = 1;
    }
    else if (P2AINP2C_b.rose())
    {
        Serial.println("1039,0");
        P2ALED2C_state = 0;
    };
    if (P2AINP3A_b.fell())
    {
        Serial.println("1040,1");
        P2ALED3A_state = 1;
    }
    else if (P2AINP3A_b.rose())
    {
        Serial.println("1040,0");
        P2ALED3A_state = 0;
    };
    if (P2AINP3B_b.fell())
    {
        Serial.println("1041,1");
        P2ALED3B_state = 1;
    }
    else if (P2AINP3B_b.rose())
    {
        Serial.println("1041,0");
        P2ALED3B_state = 0;
    };
    if (P2AINP3C_b.fell())
    {
        Serial.println("1042,1");
        P2ALED3C_state = 1;
    }
    else if (P2AINP3C_b.rose())
    {
        Serial.println("1042,0");
        P2ALED3C_state = 0;
    };
    
    //
    if (P2BINP1A_b.fell())
    {
        Serial.println("1043,1");
    }
    else if (P2BINP1A_b.rose())
    {
        Serial.println("1043,0");
    };
    if (P2BINP1B_b.fell())
    {
        Serial.println("1044,1");
    }
    else if (P2BINP1B_b.rose())
    {
        Serial.println("1044,0");
    };
    if (P2BINP2A_b.fell())
    {
        Serial.println("1045,1");
    }
    else if (P2BINP2A_b.rose())
    {
        Serial.println("1045,0");
    };
    if (P2BINP2B_b.fell())
    {
        Serial.println("1046,1");
    }
    else if (P2BINP2B_b.rose())
    {
        Serial.println("1046,0");
    };
    if (P2BINP3A_b.fell())
    {
        Serial.println("1047,1");
    }
    else if (P2BINP3A_b.rose())
    {
        Serial.println("1047,0");
    };
    if (P2BINP3B_b.fell())
    {
        Serial.println("1048,1");
    }
    else if (P2BINP3B_b.rose())
    {
        Serial.println("1048,0");
    };
    //
    if (P2CINP1A_b.fell())
    {
        Serial.println("1049,1");
        P2CLED1A_state = 1;
    }
    else if (P2CINP1A_b.rose())
    {
        Serial.println("1049,0");
        P2CLED1A_state = 0;
    };
    if (P2CINP1B_b.fell())
    {
        Serial.println("1050,1");
        P2CLED1B_state = 1;
    }
    else if (P2CINP1B_b.rose())
    {
        Serial.println("1050,0");
        P2CLED1B_state = 0;
    };
    if (P2CINP2A_b.fell())
    {
        Serial.println("1051,1");
        P2CLED2A_state = 1;
    }
    else if (P2CINP2A_b.rose())
    {
        Serial.println("1051,0");
        P2CLED2B_state = 0;
    };
    if (P2CINP2B_b.fell())
    {
        Serial.println("1052,1");
        P2CLED2B_state = 1;
    }
    else if (P2CINP2B_b.rose())
    {
        Serial.println("1052,0");
        P2CLED2B_state = 0;
    };


    // ------------------------------------------------
    // Writing to LEDS
    // ------------------------------------------------
    digitalWrite(P2ALED1A, 1);
    digitalWrite(P2ALED1B, 1);
    digitalWrite(P2ALED1C, 1);
    digitalWrite(P2ALED2A, 1);
    digitalWrite(P2ALED2B, 1);
    digitalWrite(P2ALED2C, 1);
    digitalWrite(P2ALED3A, 1);
    digitalWrite(P2ALED3B, 1);
    digitalWrite(P2ALED3C, 1);
    //
    digitalWrite(P2CLED1A, 1);
    digitalWrite(P2CLED1B, 1);
    digitalWrite(P2CLED2A, 1);
    digitalWrite(P2CLED2B, 1);
}
