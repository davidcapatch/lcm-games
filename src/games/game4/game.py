import os
import pygame
from constants import PHYSICAL_SCREEN, CLOCK_TICK_RATE, WHITE
from constants import ALIGN_LEFT, ALIGN_CENTER_H, ALIGN_RIGHT, ALIGN_TOP, ALIGN_CENTER_V, ALIGN_BOTTOM

from global_sprites import Text, Asset

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Game4():

    ''' The game class for Game 4 '''

    name = 'Game 4'
    directory = 'game4'

    def __init__(self, control_panel_queue, screen):

        # Sets self.screen to fullscreen with no frame/controls
        # ESC to exit full-self.screen
        self.screen = screen

        # self.instructions_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/menus/instructions.png')).convert_alpha()
        self.has_shown_instructions = 3
        # Create Clock
        self.clock = pygame.time.Clock()

        # Create running variable
        self.running = True

        # Pass in queue
        self.control_panel_queue = control_panel_queue

    def start(self):

        ''' Selecting a level '''

        while self.running:
            self.running = False
