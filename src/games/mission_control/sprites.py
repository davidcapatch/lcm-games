import os
import math
import pygame
from constants import PHYSICAL_SCREEN
from constants import ALIGN_LEFT, ALIGN_CENTER_H, ALIGN_RIGHT, ALIGN_TOP, ALIGN_CENTER_V, ALIGN_BOTTOM

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class BarGraph(pygame.sprite.DirtySprite):
    ''' Bar Graph Sprite
    width: 95
    height: 240
    '''

    def __init__(self, position, starting_value):

        super(BarGraph, self).__init__()

        self.position = position
        self.dirty = 1
        self.sprite_map = pygame.image.load(os.path.join(BASE_DIR, 'mission_control/resources/images/other/encoder_bars.png')).convert_alpha()
        self.image = self.get_position(starting_value)
        self.rect = pygame.Rect(self.position, (95, 240))

    def set_dirty(self):
        ''' set the sprite to dirty '''
        self.dirty = 1

    def get_position(self, value):
        '''
        input: graph number value
        output: corresponding cannon image
        '''

        if value >= 0 and value <= 9:
            location = 95*10
        elif value >= 10 and value <= 19:
            location = 95*9
        elif value >= 20 and value <= 29:
            location = 95*8
        elif value >= 30 and value <= 39:
            location = 95*7
        elif value >= 40 and value <= 49:
            location = 95*6
        elif value >= 50 and value <= 59:
            location = 95*5
        elif value >= 60 and value <= 69:
            location = 95*4
        elif value >= 70 and value <= 79:
            location = 95*3
        elif value >= 80 and value <= 89:
            location = 95*2
        elif value >= 90 and value <= 99:
            location = 95*1
        elif value == 100:
            location = 0

        rect = pygame.Rect(location, 0, 95, 240)

        return self.sprite_map.subsurface(rect).copy()

    def update(self, value):
        ''' Update the meter '''
        self.image = self.get_position(value)
        self.set_dirty()


class StatusLight(pygame.sprite.DirtySprite):
    ''' Status Light
    width: 244
    height: 140
    '''

    def __init__(self, light_number, position, starting_value):

        super(StatusLight, self).__init__()

        self.light_number = light_number * 140
        self.position = position
        self.dirty = 1
        self.sprite_map = pygame.image.load(os.path.join(BASE_DIR, 'mission_control/resources/images/other/status_lights.png')).convert_alpha()
        self.image = self.get_position(starting_value)
        self.rect = pygame.Rect(self.position, (244, 140))

    def set_dirty(self):
        ''' set the sprite to dirty '''
        self.dirty = 1

    def get_position(self, value):
        '''
        input: light status
        output: corresponding cannon image
        '''

        if value == 'off':
            location = 0
        elif value == 'green':
            location = 140*1
        elif value == 'red':
            location = 140*2
        elif value == 'blue':
            location = 140*3

        rect = pygame.Rect(location, self.light_number, 244, 140)

        return self.sprite_map.subsurface(rect).copy()

    def update(self, power):
        ''' Update the power meter '''
        self.image = self.get_position(power)
