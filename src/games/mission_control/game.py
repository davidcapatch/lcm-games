import os
import pygame
import random
from constants import (
    CLOCK_TICK_RATE,
    RED, GREEN, WHITE, GREY,

    GAME_STATE_RUNNING, GAME_STATE_PAUSED,
    GAME_STATE_COMPLETED_LEVEL, GAME_STATE_FAILED_LEVEL,
    GAME_STATE_SHOWING_INSTRUCTIONS,
    UI_LAYER,

    ALIGN_LEFT, ALIGN_CENTER_H
)

from global_sprites import Text, Asset
from .sprites import StatusLight, BarGraph
from games.projectilegravity.sprites import Button

import control_panel

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

GAME_STATE_READY_TO_LAUNCH = 10
GAME_STATE_LAUNCHING = 11
GAME_STATE_LAUNCHED = 12


class MissionControl():

    '''
    The game class for Mission Control

    Game logic
    1. Intructions
    2. Load command list
    3. Game running, do commands
    4. Ready to launch
    5. Launch
    6. Launching
    7. Launched
    '''

    name = 'Mission Control'
    directory = 'mission_control'

    def __init__(self, control_panel_queue, screen):

        # Sets self.screen to fullscreen with no frame/controls
        # ESC to exit full-self.screen
        self.screen = screen

        # Pass in queue
        self.control_panel_queue = control_panel_queue

        # set fonts
        self.bold_font = pygame.font.SysFont("Minecraftia", 20)
        self.bold_font1 = pygame.font.SysFont("Minecraftia", 30)
        self.bold_font2 = pygame.font.SysFont("Minecraftia", 40)
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 50)
        self.bold_font4 = pygame.font.SysFont("Minecraftia", 70)
        self.big_number_font = pygame.font.SysFont("Minecraftia", 100)

        # Sounds
        self.default_menu_button_sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Tiny_Button_Push.wav'))  # noqa:501
        self.nasa_countdown_sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/countdown.wav'))  # noqa:501
        self.launching_sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/launching.wav'))  # noqa:501

        # Game Menus
        menu_sprites = pygame.image.load(os.path.join(BASE_DIR, 'mission_control/resources/images/other/game_menu_sprites.png')).convert_alpha()  # noqa:501
        self.pause_menu = Asset((600, 120), menu_sprites, rect=pygame.Rect(0, 1680, 720, 840), create_dirty=False)  # noqa:501
        self.completed_menu = Asset((600, 120), menu_sprites, rect=pygame.Rect(0, 0, 720, 840), create_dirty=False)  # noqa:501
        self.failed_menu = Asset((600, 120), menu_sprites, rect=pygame.Rect(0, 840, 720, 840), create_dirty=False)  # noqa:501
        self.pause_retry_button = Button((980, 730), menu_sprites, off_rect=pygame.Rect(0, 2720, 280, 100), on_rect=pygame.Rect(840, 2720, 280, 100))  # noqa:501
        self.pause_controls_button = Button((660, 730), menu_sprites, off_rect=pygame.Rect(560, 2620, 280, 100), on_rect=pygame.Rect(840, 2620, 280, 100), on=True)  # noqa:501
        self.completed_menu_button = Button((660, 730), menu_sprites, off_rect=pygame.Rect(0, 2520, 280, 100), on_rect=pygame.Rect(280, 2520, 280, 100))  # noqa:501
        self.completed_continue_button = Button((980, 730), menu_sprites, off_rect=pygame.Rect(0, 2620, 280, 100), on_rect=pygame.Rect(280, 2620, 280, 100), on=True)  # noqa:501
        self.failed_menu_button = Button((660, 730), menu_sprites, off_rect=pygame.Rect(0, 2520, 280, 100), on_rect=pygame.Rect(560, 2520, 280, 100))  # noqa:501
        self.failed_retry_button = Button((980, 730), menu_sprites, off_rect=pygame.Rect(0, 2720, 280, 100), on_rect=pygame.Rect(280, 2720, 280, 100), on=True)  # noqa:501

        # Creating sprites
        self.all_sprites = pygame.sprite.LayeredDirty()
        self.background_image = pygame.image.load(
            os.path.join(
                BASE_DIR,
                'mission_control/resources/images/other/mc_background.png'
            )).convert_alpha()
        self.all_sprites.clear(self.screen, self.background_image)

    def game_panel_setup(self):
        for item in self.control_panel.inputs:
            if item.id == control_panel.K_KEYPAD_1A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_1B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_1C:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_2A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_2B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_2C:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_3A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_3B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_KEYPAD_3C:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_COMMUNICATION_1A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_ABORT_1A:
                item.sprite = Button((1534, 868), self.extra_asset_map, off_rect=pygame.Rect(0, 160, 170, 80), on_rect=pygame.Rect(170, 160, 170, 80))  # noqa:501

                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sprite.update(on=False)

                def temp(self):
                    self.value = 1
                    self.sprite.update(on=True)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                    self.sprite.update(on=False)
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_ABORT_1B:
                item.sprite = Button((1704, 868), self.extra_asset_map, off_rect=pygame.Rect(0, 240, 170, 80), on_rect=pygame.Rect(170, 240, 170, 80))  # noqa:501

                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sprite.update(on=False)

                def temp(self):
                    self.value = 1
                    self.sprite.update(on=True)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                    self.sprite.update(on=False)
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_ABORT_2A:
                item.sprite = Button((1534, 960), self.extra_asset_map, off_rect=pygame.Rect(0, 320, 340, 80), on_rect=pygame.Rect(340, 320, 340, 80))  # noqa:501

                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sprite.update(on=False)

                def temp(self):
                    self.value = 1
                    self.sprite.update(on=True)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                    self.sprite.update(on=False)
                item.event_up_action = temp.__get__(item)

        for item in self.game_panel.inputs:
            if item.id == control_panel.K_DOCKING_1A:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_1B:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_1C:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_2A:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_2B:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_2C:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_3A:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_3B:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_DOCKING_3C:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_CRYOGENICS_1A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_CRYOGENICS_1B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_CRYOGENICS_2A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_CRYOGENICS_2B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_CRYOGENICS_3A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_CRYOGENICS_3B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_OPERATIONS_1A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_OPERATIONS_1B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_OPERATIONS_2A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_OPERATIONS_2B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_PYROTECHNICS_1A:
                def temp(self):
                    self.value = 1

                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0

                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_PYROTECHNICS_1B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_PYROTECHNICS_2A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_PYROTECHNICS_2B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_PYROTECHNICS_3A:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_PYROTECHNICS_3B:
                def temp(self):
                    self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.value = 0
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_1A:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_1B:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_1C:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_2A:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_2B:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_2C:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_3A:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_3B:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            elif item.id == control_panel.K_BOOSTER_3C:
                def temp(self):
                    if self.value == 1:
                        self.value = 0
                    else:
                        self.value = 1
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    pass
                item.event_up_action = temp.__get__(item)
            ####
            elif item.id == control_panel.K_LIFE_SUPPORT_1B:
                item.sprite = BarGraph((442, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

            elif item.id == control_panel.K_LIFE_SUPPORT_1A:
                item.sprite = BarGraph((574, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

            elif item.id == control_panel.K_LIFE_SUPPORT_2B:
                item.sprite = BarGraph((707, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

            elif item.id == control_panel.K_LIFE_SUPPORT_2A:
                item.sprite = BarGraph((839, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

            elif item.id == control_panel.K_FUEL_1B:
                item.sprite = BarGraph((972, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

            elif item.id == control_panel.K_FUEL_1A:
                item.sprite = BarGraph((1105, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

            elif item.id == control_panel.K_FUEL_2B:
                item.sprite = BarGraph((1237, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

            elif item.id == control_panel.K_FUEL_2A:
                item.sprite = BarGraph((1370, 800), 0)
                self.bar_graphs.add(item.sprite)
                self.all_sprites.add(
                    item.sprite,
                    layer=1)
                item.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'mission_control/resources/sounds/buttons/Click2.wav'))  # noqa:501

                def temp(self):
                    self.sound.play()
                    if self.value > 0:
                        self.value -= 1
                        self.sprite.update(self.value)
                item.event_down_action = temp.__get__(item)

                def temp(self):
                    self.sound.play()
                    if self.value < 100:
                        self.value += 1
                        self.sprite.update(self.value)
                item.event_up_action = temp.__get__(item)

    def pre_start(self):
        self.instructions_image = pygame.image.load(
            os.path.join(
                BASE_DIR,
                'projectilegravity/resources/images/menus/instructions.png'
            )).convert_alpha()
        self.has_shown_instructions = 4
        # Create Clock
        self.clock = pygame.time.Clock()

        self.frame = 0
        self.timeout_counter = 0
        self.launch_counter = 11
        self.launch_timer = 0

        # Create running variable
        self.running = True
        self.game_state = GAME_STATE_RUNNING

        # Create control panel object
        self.control_panel = control_panel.CONTROL_PANEL
        self.game_panel = control_panel.GAME_PANEL

        # Instructions
        temp_image = pygame.image.load(
            os.path.join(
                BASE_DIR,
                'mission_control/resources/images/other/instructions.png'
            )).convert_alpha()
        self.instructions_page_0 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 0, 1920, 1080))  # noqa:501
        self.instructions_page_1 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 1080, 1920, 1080))  # noqa:501
        self.instructions_page_2 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 2160, 1920, 1080))  # noqa:501
        self.instructions_page_3 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 3240, 1920, 1080))  # noqa:501

        # Other assets
        self.extra_asset_map = pygame.image.load(
            os.path.join(
                BASE_DIR,
                'mission_control/resources/images/other/extras.png'
            )).convert_alpha()
        self.other_assets = pygame.sprite.Group()
        self.other_assets.add(Asset((45, 868), self.extra_asset_map, rect=pygame.Rect(0, 0, 80, 80)))  # noqa:501
        self.other_assets.add(Asset((45, 960), self.extra_asset_map, rect=pygame.Rect(0, 80, 80, 80)))  # noqa:501
        self.other_assets.add(Text((140, 878), GREY, self.bold_font2, 'EXIT', align_h=ALIGN_LEFT))  # noqa:501
        self.other_assets.add(Text((140, 970), GREY, self.bold_font2, 'MENU', align_h=ALIGN_LEFT))  # noqa:501

        self.ready_to_launch_text_1 = Text((960, 340), RED, self.bold_font4, 'READY TO', align_h=ALIGN_CENTER_H)  # noqa:501
        self.ready_to_launch_text_2 = Text((960, 440), RED, self.bold_font4, 'LAUNCH', align_h=ALIGN_CENTER_H)  # noqa:501
        self.countdown_number = Text((960, 340), RED, self.big_number_font, '10', align_h=ALIGN_CENTER_H)  # noqa:501

        # Status Lights
        self.status_lights = pygame.sprite.Group()
        self.status_lights.add(StatusLight(0, (58, 58), 'off'))
        self.status_lights.add(StatusLight(1, (312, 58), 'off'))
        self.status_lights.add(StatusLight(2, (58, 210), 'off'))
        self.status_lights.add(StatusLight(3, (312, 210), 'off'))
        self.status_lights.add(StatusLight(4, (58, 360), 'off'))
        self.status_lights.add(StatusLight(5, (312, 360), 'off'))
        self.status_lights.add(StatusLight(6, (58, 510), 'off'))
        self.status_lights.add(StatusLight(7, (312, 510), 'off'))
        self.status_lights.add(StatusLight(8, (1362, 58), 'off'))
        self.status_lights.add(StatusLight(9, (1618, 58), 'off'))
        self.status_lights.add(StatusLight(10, (1362, 210), 'off'))
        self.status_lights.add(StatusLight(11, (1618, 210), 'off'))
        self.status_lights.add(StatusLight(12, (1362, 360), 'off'))
        self.status_lights.add(StatusLight(13, (1618, 360), 'off'))
        self.status_lights.add(StatusLight(14, (1362, 510), 'off'))
        self.status_lights.add(StatusLight(15, (1618, 510), 'off'))

        # Bar graphs
        self.bar_graphs = pygame.sprite.Group()

        # Bar graph text
        self.bar_graph_text = pygame.sprite.Group()
        self.bar_graph_text.add(Text((489, 725), GREEN, self.bold_font, 'Air', align_h=ALIGN_CENTER_H))  # noqa:501
        self.bar_graph_text.add(Text((489, 750), GREEN, self.bold_font, 'Filter', align_h=ALIGN_CENTER_H))  # noqa:501

        self.bar_graph_text.add(Text((622, 725), GREEN, self.bold_font, 'Oxygen', align_h=ALIGN_CENTER_H))  # noqa:501

        self.bar_graph_text.add(Text((754, 725), GREEN, self.bold_font, 'Water', align_h=ALIGN_CENTER_H))  # noqa:501
        self.bar_graph_text.add(Text((754, 750), GREEN, self.bold_font, 'Filter', align_h=ALIGN_CENTER_H))  # noqa:501

        self.bar_graph_text.add(Text((887, 725), GREEN, self.bold_font, 'Vents', align_h=ALIGN_CENTER_H))  # noqa:501

        self.bar_graph_text.add(Text((1020, 725), GREEN, self.bold_font, 'Fuel', align_h=ALIGN_CENTER_H))  # noqa:501
        self.bar_graph_text.add(Text((1020, 750), GREEN, self.bold_font, 'Boost', align_h=ALIGN_CENTER_H))  # noqa:501

        self.bar_graph_text.add(Text((1152, 725), GREEN, self.bold_font, 'Efficiency', align_h=ALIGN_CENTER_H))  # noqa:501
        self.bar_graph_text.add(Text((1152, 750), GREEN, self.bold_font, 'Mode', align_h=ALIGN_CENTER_H))  # noqa:501

        self.bar_graph_text.add(Text((1285, 725), GREEN, self.bold_font, 'Auto', align_h=ALIGN_CENTER_H))  # noqa:501

        self.bar_graph_text.add(Text((1417, 725), GREEN, self.bold_font, 'Conserve', align_h=ALIGN_CENTER_H))  # noqa:501
        self.bar_graph_text.add(Text((1417, 750), GREEN, self.bold_font, 'PWR', align_h=ALIGN_CENTER_H))  # noqa:501

        # Instructions
        self.command_sprite_list = pygame.sprite.Group()

        #################
        # Add first batch of assets
        self.all_sprites.add(self.status_lights, layer=1)
        self.all_sprites.add(self.bar_graphs, layer=1)
        self.all_sprites.add(self.bar_graph_text, layer=1)
        self.all_sprites.add(self.command_sprite_list, layer=1)
        self.all_sprites.add(self.other_assets, layer=1)
        self.all_sprites.add(self.instructions_page_0, layer=3)
        self.instructions_page_1.set_dirty()

    def command_list_setup(self):
        # Screen coordinate locations for commands
        locations = [
            (700, 55),
            (700, 120),
            (700, 184),
            (700, 248),
            (700, 312),
            (700, 378),
            (700, 442),
            (700, 506),
            (700, 570),
            (700, 635)
        ]
        self.command_list = []
        random_commands = []

        # Pick 10 random numbers from our list of game panel inputs
        while len(random_commands) < 10:
            temp = random.randint(0, len(self.game_panel.inputs) - 1)
            if temp not in random_commands:
                random_commands.append(temp)

        for i, command in enumerate(random_commands):
            self.command_list.append(
                [self.game_panel.inputs[command].id, 0]
            )
            text = self.game_panel.inputs[command].name

            # set target
            if self.game_panel.inputs[command].type == 'button':
                self.game_panel.inputs[command].target = 1
                text = 'Activate ' + text
            elif self.game_panel.inputs[command].type == 'switch':
                self.game_panel.inputs[command].target = 1
                text = 'Activate ' + text
            elif self.game_panel.inputs[command].type == 'encoder':
                self.game_panel.inputs[command].target = random.choice(
                    [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100])
                text = 'Set ' + text + ' to ' + str(self.game_panel.inputs[command].target)  # noqa:501

            # Set the command we want to do
            self.game_panel.inputs[command].command_text_sprite = Text(
                locations[i],
                WHITE,
                self.bold_font1,
                text,
                align_h=ALIGN_LEFT
            )
            self.command_sprite_list.add(
                self.game_panel.inputs[command].command_text_sprite)
            self.all_sprites.add(
                self.game_panel.inputs[command].command_text_sprite,
                layer=1)

    def command_list_check(self):
        pass

    def draw_level_cleared(self):
        """Draw self.level cleared"""

        # remove commands
        for item in self.command_sprite_list:
            self.command_sprite_list.remove(item)
            self.all_sprites.remove(item)

        self.game_state = GAME_STATE_READY_TO_LAUNCH
        self.all_sprites.add(self.ready_to_launch_text_1, layer=2)
        self.all_sprites.add(self.ready_to_launch_text_2, layer=2)

    def clean_up(self):
        for item in self.game_panel.inputs:
            item.value = 0
        # remove commands
        for item in self.command_sprite_list:
            self.command_sprite_list.remove(item)
            self.all_sprites.remove(item)

        self.all_sprites.remove_sprites_of_layer(2)
        self.all_sprites.remove_sprites_of_layer(UI_LAYER)

        # Stop all sounds
        pygame.mixer.stop()

    def launch(self):
        self.game_state = GAME_STATE_LAUNCHING
        self.nasa_countdown_sound.play()
        self.all_sprites.remove(self.ready_to_launch_text_1)
        self.all_sprites.remove(self.ready_to_launch_text_2)

        self.countdown_number.update(str(self.launch_counter))
        self.all_sprites.add(self.countdown_number, layer=2)

    def launched(self):
        self.game_state = GAME_STATE_COMPLETED_LEVEL

        self.menu_button = 2
        self.completed_menu.set_dirty()
        self.completed_continue_button.update(on=True)
        self.completed_menu_button.update(on=False)
        self.all_sprites.add(self.completed_menu, layer=UI_LAYER)
        self.all_sprites.add(self.completed_continue_button, layer=UI_LAYER)
        self.all_sprites.add(self.completed_menu_button, layer=UI_LAYER)

    def is_done_launching(self):
        if self.launch_counter >= 0:
            self.countdown_number.update(str(self.launch_counter))

        if self.launch_timer > 10:
            self.all_sprites.remove(self.countdown_number)
            # self.nasa_countdown_sound.fadeout(200)
            if not self.launching_sound.get_num_channels():
                self.launching_sound.play()

        if self.launch_timer >= 20:
            self.launched()

    def restart(self):
        self.clean_up()
        self.command_list_setup()

    def start(self):

        # Run pre start
        self.pre_start()
        self.game_panel_setup()
        self.command_list_setup()

        ''' Selecting a level '''

        while self.running:

            if self.frame == CLOCK_TICK_RATE:
                self.frame = 0
                self.timeout_counter += 1
                if self.game_state == GAME_STATE_LAUNCHING:
                    self.launch_counter -= 1
                    self.launch_timer += 1
            else:
                self.frame += 1

            while not self.control_panel_queue.empty():
                new_event = self.control_panel_queue.get()
                pygame.event.post(pygame.event.Event(new_event[0], key=new_event[1]))  # noqa:501

            # Input handling
            for event in pygame.event.get():

                try:
                    event.key
                except AttributeError:
                    continue

                if event.type == pygame.QUIT:
                    self.running = False
                    self.clean_up()

                # Ignore any mouse motion
                elif event.type == pygame.MOUSEMOTION:
                    continue

                elif (event.type == pygame.KEYDOWN and event.key == pygame.K_BACKSPACE) or (event.type == pygame.KEYDOWN and event.key == control_panel.K_KEYPAD_1C):  # noqa:501

                    self.running = False
                    self.clean_up()

                # Hide instructions after first button input
                elif self.has_shown_instructions:
                    if event.type == pygame.KEYDOWN:
                        self.default_menu_button_sound.play()
                        if self.has_shown_instructions == 4:
                            self.all_sprites.remove(self.instructions_page_0)  # noqa:501
                            self.all_sprites.add(self.instructions_page_1, layer=2)  # noqa:501
                            self.instructions_page_1.set_dirty()
                            self.has_shown_instructions = 3
                        elif self.has_shown_instructions == 3:
                            self.all_sprites.remove(self.instructions_page_1)  # noqa:501
                            self.all_sprites.add(self.instructions_page_2, layer=2)  # noqa:501
                            self.instructions_page_2.set_dirty()
                            self.has_shown_instructions = 2
                        elif self.has_shown_instructions == 2:
                            self.all_sprites.remove(self.instructions_page_2)  # noqa:501
                            self.all_sprites.add(self.instructions_page_3, layer=2)  # noqa:501
                            self.instructions_page_3.set_dirty()
                            self.has_shown_instructions = 1
                        elif self.has_shown_instructions == 1:
                            self.all_sprites.remove(self.instructions_page_3)  # noqa:501
                            self.has_shown_instructions = 0

                elif self.game_state == GAME_STATE_READY_TO_LAUNCH:
                    if event.type == pygame.KEYDOWN and (event.key == pygame.K_RETURN or event.key == control_panel.K_ENTER_2A):  # noqa:501
                        self.launch()
                elif self.game_state == GAME_STATE_LAUNCHING:
                    # Eat inputs
                    continue

                elif event.key == pygame.K_RETURN or event.key == control_panel.K_ENTER_2A:  # noqa:501
                    if event.type == pygame.KEYDOWN:
                        if self.game_state == GAME_STATE_RUNNING:
                            pass
                        elif self.game_state == GAME_STATE_PAUSED:
                            self.default_menu_button_sound.play()
                            if self.menu_button == 1:
                                # Show instructions
                                self.game_state = GAME_STATE_SHOWING_INSTRUCTIONS  # noqa:501
                                self.instructions_menu.set_dirty()
                                self.all_sprites.add(self.instructions_menu, layer=UI_LAYER)  # noqa:501
                            else:
                                # Go to level select
                                self.restart()
                                self.game_state = GAME_STATE_RUNNING

                        elif self.game_state == GAME_STATE_COMPLETED_LEVEL:
                            self.default_menu_button_sound.play()
                            if self.menu_button == 1:
                                # Quit
                                self.game_state = GAME_STATE_RUNNING
                                self.clean_up()
                                self.running = False
                                break
                            else:
                                # Play Again
                                self.game_state = GAME_STATE_RUNNING
                                self.restart()
                        elif self.game_state == GAME_STATE_FAILED_LEVEL:
                            self.default_menu_button_sound.play()
                            if self.menu_button == 1:
                                # Go to level select
                                self.running = False
                                break
                            else:
                                # Reload level
                                self.restart()

                # Pause
                elif event.key == pygame.K_TAB or event.key == control_panel.K_KEYPAD_3C:  # noqa:501
                    if event.type == pygame.KEYDOWN:
                        self.default_menu_button_sound.play()
                        if self.game_state == GAME_STATE_RUNNING:
                            # Pause
                            self.game_state = GAME_STATE_PAUSED
                            self.menu_button = 2
                            self.pause_menu.set_dirty()
                            self.pause_controls_button.update(on=False)
                            self.pause_retry_button.update(on=True)
                            self.all_sprites.add(self.pause_menu, layer=UI_LAYER)  # noqa:501
                            self.all_sprites.add(self.pause_retry_button, layer=UI_LAYER)  # noqa:501
                            self.all_sprites.add(self.pause_controls_button, layer=UI_LAYER)  # noqa:501
                        elif self.game_state == GAME_STATE_PAUSED:
                            # Un pause
                            self.game_state = GAME_STATE_RUNNING
                            self.all_sprites.remove(self.pause_menu)
                            self.all_sprites.remove(self.pause_retry_button)
                            self.all_sprites.remove(self.pause_controls_button)
                        elif self.game_state == GAME_STATE_SHOWING_INSTRUCTIONS:  # noqa:501
                            self.game_state = GAME_STATE_PAUSED
                            self.all_sprites.remove(self.instructions_menu)

                # Menu option selecting
                elif event.key == pygame.K_RIGHT or event.key == control_panel.K_KEYPAD_2A:  # noqa:501
                    if event.type == pygame.KEYDOWN:
                        self.default_menu_button_sound.play()
                        self.menu_button = 2
                        if self.game_state == GAME_STATE_PAUSED:
                            if self.menu_button == 2:
                                self.pause_controls_button.update(on=False)
                                self.pause_retry_button.update(on=True)
                        elif self.game_state == GAME_STATE_COMPLETED_LEVEL:
                            if self.menu_button == 2:
                                self.completed_continue_button.update(on=True)
                                self.completed_menu_button.update(on=False)
                        elif self.game_state == GAME_STATE_FAILED_LEVEL:
                            if self.menu_button == 2:
                                self.failed_retry_button.update(on=True)
                                self.failed_menu_button.update(on=False)

                elif event.key == pygame.K_LEFT or event.key == control_panel.K_KEYPAD_2C:  # noqa:501
                    if event.type == pygame.KEYDOWN:
                        self.default_menu_button_sound.play()
                        self.menu_button = 1
                        if self.game_state == GAME_STATE_PAUSED:
                            if self.menu_button == 1:
                                self.pause_controls_button.update(on=True)
                                self.pause_retry_button.update(on=False)
                        elif self.game_state == GAME_STATE_COMPLETED_LEVEL:
                            if self.menu_button == 1:
                                self.completed_continue_button.update(on=False)
                                self.completed_menu_button.update(on=True)
                        elif self.game_state == GAME_STATE_FAILED_LEVEL:
                            if self.menu_button == 1:
                                self.failed_retry_button.update(on=False)
                                self.failed_menu_button.update(on=True)

                # testing
                elif event.key == pygame.K_a:
                    if event.type == pygame.KEYUP:
                        thing = self.game_panel.get_input(id=control_panel.K_LIFE_SUPPORT_1B)  # noqa:501
                        thing.event_up_action()
                elif event.key == pygame.K_s:
                    if event.type == pygame.KEYUP:
                        thing = self.game_panel.get_input(id=control_panel.K_LIFE_SUPPORT_1A)  # noqa:501
                        thing.event_up_action()

                ##########################
                else:
                    processed = False
                    for input_item in self.control_panel.inputs:
                        if event.key == input_item.id:
                            if event.type == pygame.KEYDOWN:
                                input_item.event_down_action()
                                processed = True
                                continue
                            else:
                                input_item.event_up_action()
                                processed = True
                                continue

                    if not processed:
                        for input_item in self.game_panel.inputs:
                            if event.key == input_item.id:
                                if event.type == pygame.KEYDOWN:
                                    input_item.event_down_action()
                                    continue
                                else:
                                    input_item.event_up_action()
                                    continue

            # Check statuses
            for item in self.command_list:
                input_object = self.game_panel.get_input(id=item[0])

                if input_object.type in ['button', 'switch']:
                    if input_object.value == input_object.target:
                        # Set command to green
                        input_object.command_text_sprite.update_color(
                            GREEN
                        )
                        item[1] = 1
                elif input_object.type == 'encoder':
                    if input_object.value >= input_object.target:
                        # Set command to green
                        input_object.command_text_sprite.update_color(
                            GREEN
                        )
                        item[1] = 1

            if self.game_state == GAME_STATE_RUNNING:

                level_complete = True
                for item in self.command_list:
                    if item[1] == 0:
                        level_complete = False

                # Display level complete
                if level_complete:
                    self.draw_level_cleared()
            elif self.game_state == GAME_STATE_LAUNCHING:
                self.is_done_launching()

            dirty_rects = self.all_sprites.draw(self.screen)
            pygame.display.update(dirty_rects)

            self.clock.tick(CLOCK_TICK_RATE)

            # pygame.display.set_caption(self.name)
