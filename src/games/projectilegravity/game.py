import os
import pygame
from constants import PHYSICAL_SCREEN, CLOCK_TICK_RATE, WHITE
from constants import ALIGN_LEFT, ALIGN_CENTER_H, ALIGN_RIGHT, ALIGN_TOP, ALIGN_CENTER_V, ALIGN_BOTTOM
# from constants import UI_LAYER
from .planets import Mercury, Venus, Earth, ISS, Moon, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto

from global_sprites import Text, Asset
from control_panel import (
    K_CONTROL_1A,
    K_CONTROL_1A_CLICK,
    K_CONTROL_1B,
    K_CONTROL_1B_CLICK,
    K_ENTER_2A,
    K_KEYPAD_1A,
    K_KEYPAD_1B,
    K_KEYPAD_1C,
    K_KEYPAD_2A,
    K_KEYPAD_2B,
    K_KEYPAD_2C,
    K_KEYPAD_3A,
    K_KEYPAD_3B,
    K_KEYPAD_3C,
)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def init_planets():
    ''' Initialize all the planets '''
    planets = [Mercury(), Venus(), Earth(), ISS(), Moon(), Mars(), Jupiter(), Saturn(), Uranus(), Neptune(), Pluto()]
    return planets


class ProjectileGravity():

    ''' The game class for Projectile Gravity '''

    name = 'Projectile Gravity'
    directory = 'projectilegravity'

    def __init__(self, control_panel_queue, screen):

        # Sets self.screen to fullscreen with no frame/controls
        # ESC to exit full-self.screen
        self.screen = screen

        self.instructions_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/menus/instructions.png')).convert_alpha()
        self.has_shown_instructions = 3
        # Create Clock
        self.clock = pygame.time.Clock()

        # Create running variable
        self.running = True

        # Next game level storage variable
        self.next_level = None

        # Pass in queue
        self.control_panel_queue = control_panel_queue

        # Level Select
        self.hovered_planet = 0
        self.selected_planet = None
        self.hovered_sub_planet = 0
        self.selected_sub_planet = None
        self.hovered_level = 0
        self.selected_level = None
        self.planets = init_planets()
        self.hovering_levels = True
        self.bold_font = pygame.font.SysFont("Minecraftia", 30)
        self.bold_font2 = pygame.font.SysFont("Minecraftia", 40)
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 50)

        # Creating sprites
        self.all_sprites = pygame.sprite.LayeredDirty()
        self.background_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_select_backgrounds/planet_menu_bg.png')).convert_alpha()
        self.all_sprites.clear(self.screen, self.background_image)

        # Instructions
        temp_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/menus/instructions.png')).convert_alpha()
        self.instructions_page_0 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 0, 1920, 1080))
        self.instructions_page_1 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 1080, 1920, 1080))
        self.instructions_page_2 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 2160, 1920, 1080))
        self.instructions_page_3 = Asset((0, 0), temp_image, rect=pygame.Rect(0, 3240, 1920, 1080))

        # Other assets
        selector_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_selector/selector.png')).convert_alpha()
        self.selector = Asset((355, 115), selector_image, create_dirty=False)

        level_1_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
        self.level_1 = Asset((430, 190), level_1_image, create_dirty=False)

        # Planet level header text
        self.selected_planet_text = Text((PHYSICAL_SCREEN.width / 2, 50), WHITE, self.bold_font3, '', align_h=ALIGN_CENTER_H)

        #################
        # Add first batch of assets
        self.all_sprites.add(self.planets[0].sprites, layer=1)
        self.all_sprites.add(self.instructions_page_1, layer=2)
        self.instructions_page_1.set_dirty()

    def load_music(self):
        """Load the music"""
        pygame.mixer.music.load(os.path.join(BASE_DIR, 'projectilegravity/resources/sounds/angry-birds.ogg'))
        pygame.mixer.music.play(-1)

    def start(self):

        ''' Selecting a level '''

        default_menu_button_sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'projectilegravity/resources/sounds/buttons/Tiny_Button_Push.wav'))  # noqa:501

        while self.running:

            # If we are coming out of one level and heading into another
            # we immediately load into it
            if self.next_level:
                self.next_level = self.next_level(control_panel_queue=self.control_panel_queue)
                self.next_level = self.next_level.load_level()

            while not self.control_panel_queue.empty():
                new_event = self.control_panel_queue.get()
                pygame.event.post(pygame.event.Event(new_event[0], key=new_event[1]))


            # Input handling
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif (event.type == pygame.KEYDOWN and event.key == pygame.K_BACKSPACE) or (event.type == pygame.KEYDOWN and event.key == K_KEYPAD_1C):

                    if self.selected_planet:
                        self.selected_planet = None
                        self.all_sprites.empty()
                        self.all_sprites.add(self.planets[self.hovered_planet].sprites)
                        self.planets[self.hovered_planet].set_sprites_dirty()

                    else:
                        self.running = False

                elif event.type == pygame.KEYDOWN:

                    default_menu_button_sound.play()

                    # Hide instructions after first button input
                    if self.has_shown_instructions:
                        if self.has_shown_instructions == 3:
                            self.all_sprites.remove(self.instructions_page_1)
                            self.all_sprites.add(self.instructions_page_2, layer=2)
                            self.instructions_page_2.set_dirty()
                            self.has_shown_instructions = 2
                        elif self.has_shown_instructions == 2:
                            self.all_sprites.remove(self.instructions_page_2)
                            self.all_sprites.add(self.instructions_page_3, layer=2)
                            self.instructions_page_3.set_dirty()
                            self.has_shown_instructions = 1
                        elif self.has_shown_instructions == 1:
                            self.all_sprites.remove(self.instructions_page_3)
                            self.has_shown_instructions = 0

                    else:

                        if event.key == pygame.K_RIGHT or event.key == K_KEYPAD_2A:

                            # Choosing a planet
                            if not self.selected_planet:
                                if self.hovered_planet + 1 == len(self.planets):
                                    self.hovered_planet = 0
                                else:
                                    self.hovered_planet += 1

                            # Else choosing a sub planet or level
                            else:
                                if self.hovered_level + 1 == len(self.selected_planet.levels):
                                    self.hovered_level = 0
                                else:
                                    self.hovered_level += 1

                            self.all_sprites.empty()
                            self.all_sprites.add(self.planets[self.hovered_planet].sprites)
                            self.planets[self.hovered_planet].set_sprites_dirty()

                        elif event.key == pygame.K_LEFT or event.key == K_KEYPAD_2C:

                            # Choosing a planet
                            if not self.selected_planet:
                                if self.hovered_planet - 1 < 0:
                                    self.hovered_planet = len(self.planets) - 1
                                else:
                                    self.hovered_planet -= 1

                            # Else choosing a  level
                            else:
                                if self.hovered_level - 1 < 0:
                                    self.hovered_level = len(self.selected_planet.levels)
                                else:
                                    self.hovered_level -= 1

                            self.all_sprites.empty()
                            self.all_sprites.add(self.planets[self.hovered_planet].sprites)
                            self.planets[self.hovered_planet].set_sprites_dirty()

                        elif event.key == pygame.K_DOWN:
                            pass

                        elif event.key == pygame.K_UP:
                            pass

                        elif event.key == pygame.K_RETURN or event.key == K_ENTER_2A:

                            # Choose a planet
                            if not self.selected_planet:
                                self.selected_sub_planet = None
                                self.selected_level = None
                                self.hovering_levels = True

                                self.selected_planet = self.planets[self.hovered_planet]

                                self.all_sprites.empty()
                                self.all_sprites.add(self.selector)
                                self.all_sprites.add(self.level_1)
                                self.all_sprites.add(self.selected_planet_text)
                                self.selected_planet_text.update(self.selected_planet.name + ' Levels')
                                self.selector.set_dirty()
                                self.level_1.set_dirty()

                            # Choosing sub planet or level
                            else:
                                self.selected_level = self.selected_planet.levels[self.hovered_level]
                                # Start selected game
                                print('Start Level 1', self.selected_level)
                                # game_state = GAME_STATE_LEVEL_LOADED
                                level = self.selected_level(control_panel_queue=self.control_panel_queue)
                                self.next_level = level.load_level()

                                if not self.next_level:
                                    self.screen.blit(self.background_image, (0, 0))
                                    pygame.display.flip()
                                    for sprite in self.all_sprites:
                                        sprite.set_dirty()

                                    dirty_rects = self.all_sprites.draw(self.screen)
                                    pygame.display.update(dirty_rects)

                                elif self.next_level == 'TIMEOUT':
                                    self.running = False

            dirty_rects = self.all_sprites.draw(self.screen)
            pygame.display.update(dirty_rects)

            self.clock.tick(CLOCK_TICK_RATE)

            # pygame.display.set_caption(self.name)
