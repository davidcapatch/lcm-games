import os
import math
import pygame
import pymunk as pm
from constants import CLOCK_TICK_RATE, PHYSICAL_SCREEN
from constants import WHITE, BLACK, BLUE
from constants import GAME_STATE_RUNNING, GAME_STATE_PAUSED, GAME_STATE_COMPLETED_LEVEL, GAME_STATE_COMPLETED_FINAL_LEVEL, GAME_STATE_FAILED_LEVEL, GAME_STATE_SHOWING_INSTRUCTIONS
from constants import ALIGN_LEFT, ALIGN_CENTER_H, ALIGN_RIGHT, ALIGN_TOP, ALIGN_CENTER_V, ALIGN_BOTTOM
from constants import BALL_LAYER, BLOCK_LAYER, PLATFORM_LAYER, UI_LAYER
from control_panel import (
    K_CONTROL_1A,
    K_CONTROL_1A_CLICK,
    K_CONTROL_1B,
    K_CONTROL_1B_CLICK,
    K_ENTER_2A,
    K_KEYPAD_1A,
    K_KEYPAD_1B,
    K_KEYPAD_1C,
    K_KEYPAD_2A,
    K_KEYPAD_2B,
    K_KEYPAD_2C,
    K_KEYPAD_3A,
    K_KEYPAD_3B,
    K_KEYPAD_3C,
)

from global_sprites import Text, Asset
from .sprites import Ball, GreenBlock, RedBlock, Platform, Cannon, PowerMeter, Button

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

BASE_GRAVITY = -1500

def to_pygame(pos):
    """Convert pymunk to pygame coordinates"""
    return int(pos.x), int(-pos.y+600)


def vector(p_0, p_1):
    """Return the vector of the points
    p0 = (xo,yo), p1 = (x1,y1)"""
    a = p_1[0] - p_0[0]
    b = p_1[1] - p_0[1]
    return (a, b)


class Level():
    '''
    This is the main class the defines a game level

    Subclass to create a new level
    '''

    space = pm.Space()
    screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN | pygame.NOFRAME)
    # screen = pygame.display.set_mode([1920, 1080])

    name = ''
    gravity = (0.0, -700.0)

    platforms_to_create = []
    walls_to_create = []
    green_blocks_to_create = []
    green_blocks_num = 0
    red_blocks_to_create = []
    red_blocks_num = 0
    has_ground = False
    lowest_platform_height = -200

    shot_angle = 22
    shot_power = 50
    shot_rads = math.radians(shot_angle)

    balls = []


    background = None
    level_image = None
    sub_planets = []
    next_level = None

    def __init__(self, control_panel_queue):

        # Loading images
        self.instructions_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/menus/instructions.png')).convert_alpha()
        # self.ball = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/objects/ball.png')).convert_alpha()
        self.green_block = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/objects/green-block.png')).convert_alpha()
        self.red_block = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/objects/red-block.png')).convert_alpha()
        # self.static_block = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/objects/static-block.png')).convert_alpha()

        menu_sprites = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/menus/game_menu_sprites.png')).convert_alpha()

        self.running = False

        # Menu button
        self.menu_button = 2

        # Pass in queue
        self.control_panel_queue = control_panel_queue

        # Create Clock
        self.clock = pygame.time.Clock()
        self.frame = 0

        # PHYSICS
        self.space = pm.Space()
        self.space.sleep_time_threshold = 2 # This may put bodies to sleep after 2 seconds of idle (based on gravity)?
        self.space.gravity = self.gravity
        self.floor_height = 200
        self.wall = False
        self.platforms = pygame.sprite.Group()
        self.walls = pygame.sprite.Group()
        self.green_blocks = pygame.sprite.Group()
        self.red_blocks = pygame.sprite.Group()
        self.balls = pygame.sprite.Group()
        self.ball_number = 0
        self.last_fired_ball = None
        self.ui_elements = pygame.sprite.Group()
        self.all_sprites = pygame.sprite.LayeredDirty()

        self.shot_angle = 45
        self.power_pot = None
        self.angle_pot = None
        self.shot_power = 50
        self.shot_rads = math.radians(self.shot_angle)

        # Game Variables
        self.game_state = GAME_STATE_RUNNING
        self.timeout_value = 300 # 5 minutes
        self.timeout_counter = 0

        # Sounds
        self.sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'projectilegravity/resources/sounds/launchy.wav'))
        # self.sound.set_volume(0.1)
        self.default_menu_button_sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'projectilegravity/resources/sounds/buttons/Tiny_Button_Push.wav'))  # noqa:501


        # Text
        self.bold_font = pygame.font.SysFont("Minecraftia", 30)
        self.bold_font2 = pygame.font.SysFont("Minecraftia", 40)
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 50)

        self.static_body = pm.Body(body_type=pm.Body.STATIC)

        if self.has_ground:
            self.ground = pm.Segment(self.static_body, (0.0, -((PHYSICAL_SCREEN.height/2)-self.floor_height)), (PHYSICAL_SCREEN.width, -((PHYSICAL_SCREEN.height/2)-self.floor_height)), 0.0)
            self.ground.friction = 0.9
            self.space.add(self.ground)

        # Construct sprites
        # UI
        self.title_text = Text((PHYSICAL_SCREEN.width / 2, 50), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.cannon = Cannon(self.floor_height, self.shot_angle)
        self.power_meter = PowerMeter(self.floor_height, self.shot_power)
        self.angle_label = Text((100, 950), WHITE, self.bold_font, "Angle")
        self.angle_text = Text((215, 950), WHITE, self.bold_font, self.shot_angle)
        self.green_block_text = Text((1780, 945), WHITE, self.bold_font2, self.green_blocks_num)
        self.static_green_block = Asset((1830, 950), self.green_block)
        if self.red_blocks_num:
            self.red_block_text = Text((1780, 1000), WHITE, self.bold_font2, self.red_blocks_num)
            self.static_red_block = Asset((1830, 1000), self.red_block)
            self.ui_elements.add(self.red_block_text)
            self.ui_elements.add(self.static_red_block)
        # Menu items
        self.instructions_menu = Asset((0, 0), self.instructions_image, rect=pygame.Rect(0, 0, 1920, 1080), create_dirty=False)
        self.pause_menu = Asset((600, 120), menu_sprites, rect=pygame.Rect(0, 1680, 720, 840), create_dirty=False)
        self.completed_menu = Asset((600, 120), menu_sprites, rect=pygame.Rect(0, 0, 720, 840), create_dirty=False)
        self.failed_menu = Asset((600, 120), menu_sprites, rect=pygame.Rect(0, 840, 720, 840), create_dirty=False)
        self.pause_menu_button = Button((660, 730), menu_sprites, off_rect=pygame.Rect(0, 2720, 280, 100), on_rect=pygame.Rect(840, 2720, 280, 100))
        self.pause_controls_button = Button((980, 730), menu_sprites, off_rect=pygame.Rect(560, 2620, 280, 100), on_rect=pygame.Rect(840, 2620, 280, 100), on=True)
        self.completed_menu_button = Button((660, 730), menu_sprites, off_rect=pygame.Rect(0, 2520, 280, 100), on_rect=pygame.Rect(280, 2520, 280, 100))
        self.completed_continue_button = Button((980, 730), menu_sprites, off_rect=pygame.Rect(0, 2620, 280, 100), on_rect=pygame.Rect(280, 2620, 280, 100), on=True)
        self.failed_menu_button = Button((660, 730), menu_sprites, off_rect=pygame.Rect(0, 2520, 280, 100), on_rect=pygame.Rect(560, 2520, 280, 100))
        self.failed_retry_button = Button((980, 730), menu_sprites, off_rect=pygame.Rect(0, 2720, 280, 100), on_rect=pygame.Rect(280, 2720, 280, 100), on=True)

        self.ui_elements.add(self.title_text)
        self.ui_elements.add(self.cannon)
        self.ui_elements.add(self.power_meter)
        self.ui_elements.add(self.angle_label)
        self.ui_elements.add(self.angle_text)
        self.ui_elements.add(self.green_block_text)
        self.ui_elements.add(self.static_green_block)

        for platform in self.platforms_to_create:
            self.platforms.add(Platform(platform, self.space))
        # for wall in self.walls_to_create:
        #     self.platforms.add(FixedPolygon(wall, self.space))
        for block in self.green_blocks_to_create:
            self.green_blocks.add(GreenBlock(block, self.space))
        for block in self.red_blocks_to_create:
            self.red_blocks.add(RedBlock(block, self.space))

        self.all_sprites.add(self.balls, layer=BALL_LAYER)
        self.all_sprites.add(self.green_blocks, layer=BLOCK_LAYER)
        self.all_sprites.add(self.red_blocks, layer=BLOCK_LAYER)
        self.all_sprites.add(self.platforms, layer=PLATFORM_LAYER)
        self.all_sprites.add(self.ui_elements, layer=UI_LAYER)
        self.all_sprites.clear(self.screen, self.background)

        # Collision handler
        # self.space.add_collision_handler(0, 1).post_solve = self.post_solve_ball_block
        # self.space.add_collision_handler(0, 2).post_solve = self.post_solve_ball_platform
        # self.space.add_collision_handler(1, 2).post_solve = self.post_solve_block_platform

        # self.load_music()


    def is_off_screen(self, x, y, gravity):
        ''' Check if the object is off the screen and
            will not be able to return
        '''

        buffer = 50 # Do not use the exact boundaries

        # NOT SPACE
        if gravity[1] != BASE_GRAVITY * 0.0000001:
            if y < -(PHYSICAL_SCREEN.height/2) - buffer:
                # print('Off the bottom')
                return True
            if x < 0 - buffer:
                # print('Off the left side')
                return True
            if x > PHYSICAL_SCREEN.width + buffer:
                # print('Off the right side')
                return True
        # SPACE
        else:
            if y < -(PHYSICAL_SCREEN.height/2) - buffer:
                # print('Off the bottom')
                return True
            if y > (PHYSICAL_SCREEN.height/2) + buffer:
                # print('Off the top')
                return True
            if x < 0 - buffer:
                # print('Off the left side')
                return True
            if x > PHYSICAL_SCREEN.width + buffer:
                # print('Off the right side')
                return True

        return False

    def handle_input_events(self, events):
        '''
        Handle pygame inputs
        '''

        while not self.control_panel_queue.empty():
            new_event = self.control_panel_queue.get()
            pygame.event.post(pygame.event.Event(new_event[0], key=new_event[1]))

        for event in events:
            # Reset timeout timer
            self.timeout_counter = 0

            try:
                event.key
            except AttributeError:
                continue

            if event.type == pygame.QUIT:
                self.running = False
                break
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE or event.key == K_KEYPAD_1C:
                    self.running = False
                    break                

                # Select / FIRE
                elif event.key == pygame.K_RETURN or event.key == K_ENTER_2A:
                    if self.game_state == GAME_STATE_RUNNING:
                        self.fire()
                    elif self.game_state == GAME_STATE_PAUSED:
                        self.default_menu_button_sound.play()
                        if self.menu_button == 1:
                            # Go to level select
                            self.restart()
                            self.game_state = GAME_STATE_RUNNING
                            self.all_sprites.remove(self.pause_menu)
                            self.all_sprites.remove(self.pause_menu_button)
                            self.all_sprites.remove(self.pause_controls_button)
                        else:
                            # Show instructions
                            self.game_state = GAME_STATE_SHOWING_INSTRUCTIONS
                            self.instructions_menu.set_dirty()
                            self.all_sprites.add(self.instructions_menu, layer=UI_LAYER)
                    elif self.game_state == GAME_STATE_COMPLETED_LEVEL:
                        self.default_menu_button_sound.play()
                        if self.menu_button == 1:
                            # Go to level select
                            self.running = False
                            break
                        else:
                            # Go to next level
                            # return self.next_level
                            self.running = False
                    elif self.game_state == GAME_STATE_FAILED_LEVEL:
                        self.default_menu_button_sound.play()
                        if self.menu_button == 1:
                            # Go to level select
                            self.running = False
                            break
                        else:
                            # Reload level
                            self.restart()

                # Pause
                elif event.key == pygame.K_TAB or event.key == K_KEYPAD_3C:
                    self.default_menu_button_sound.play()
                    if self.game_state == GAME_STATE_RUNNING:
                        # Pause
                        self.game_state = GAME_STATE_PAUSED
                        self.menu_button = 2
                        self.pause_menu.set_dirty()
                        self.pause_controls_button.update(on=True)
                        self.pause_menu_button.update(on=False)
                        self.all_sprites.add(self.pause_menu, layer=UI_LAYER)
                        self.all_sprites.add(self.pause_menu_button, layer=UI_LAYER)
                        self.all_sprites.add(self.pause_controls_button, layer=UI_LAYER)
                    elif self.game_state == GAME_STATE_PAUSED:
                        # Un pause
                        self.game_state = GAME_STATE_RUNNING
                        self.all_sprites.remove(self.pause_menu)
                        self.all_sprites.remove(self.pause_menu_button)
                        self.all_sprites.remove(self.pause_controls_button)
                    elif self.game_state == GAME_STATE_SHOWING_INSTRUCTIONS:
                        self.game_state = GAME_STATE_PAUSED
                        self.all_sprites.remove(self.instructions_menu)

                # Menu option selecting
                elif event.key == pygame.K_RIGHT or event.key == K_KEYPAD_2A:
                    self.default_menu_button_sound.play()
                    self.menu_button = 2
                    if self.game_state == GAME_STATE_PAUSED:
                        if self.menu_button == 2:
                            self.pause_controls_button.update(on=True)
                            self.pause_menu_button.update(on=False)
                    elif self.game_state == GAME_STATE_COMPLETED_LEVEL:
                        if self.menu_button == 2:
                            self.completed_continue_button.update(on=True)
                            self.completed_menu_button.update(on=False)
                    elif self.game_state == GAME_STATE_FAILED_LEVEL:
                        if self.menu_button == 2:
                            self.failed_retry_button.update(on=True)
                            self.failed_menu_button.update(on=False)

                elif event.key == pygame.K_LEFT or event.key == K_KEYPAD_2C:
                    self.default_menu_button_sound.play()
                    self.menu_button = 1
                    if self.game_state == GAME_STATE_PAUSED:
                        if self.menu_button == 1:
                            self.pause_controls_button.update(on=False)
                            self.pause_menu_button.update(on=True)
                    elif self.game_state == GAME_STATE_COMPLETED_LEVEL:
                        if self.menu_button == 1:
                            self.completed_continue_button.update(on=False)
                            self.completed_menu_button.update(on=True)
                    elif self.game_state == GAME_STATE_FAILED_LEVEL:
                        if self.menu_button == 1:
                            self.failed_retry_button.update(on=False)
                            self.failed_menu_button.update(on=True)

            # Angle
            if event.key == K_CONTROL_1A:
                if event.type == pygame.KEYUP:
                    self.cannon.set_dirty()
                    self.angle_text.set_dirty()
                    if self.shot_angle < 90:
                        self.shot_angle += 1
                    self.shot_rads = math.radians(self.shot_angle)
                else:
                    self.cannon.set_dirty()
                    self.angle_text.set_dirty()
                    if self.shot_angle > 0:
                        self.shot_angle -= 1
                    self.shot_rads = math.radians(self.shot_angle)

            # DEBUG
            if event.key == pygame.K_a:
                if event.type == pygame.KEYDOWN:
                    self.cannon.set_dirty()
                    self.angle_text.set_dirty()
                    if self.shot_angle < 90:
                        self.shot_angle += 1
                    self.shot_rads = math.radians(self.shot_angle)
            elif event.key == pygame.K_z:
                if event.type == pygame.KEYDOWN:
                    self.cannon.set_dirty()
                    self.angle_text.set_dirty()
                    if self.shot_angle > 0:
                        self.shot_angle -= 1
                    self.shot_rads = math.radians(self.shot_angle)

            # Power
            elif event.key == K_CONTROL_1B:
                if event.type == pygame.KEYDOWN:
                    self.power_meter.set_dirty()
                    if self.shot_power > 1:
                        self.shot_power -= 1
                else:
                    self.power_meter.set_dirty()
                    if self.shot_power < 101:
                        self.shot_power += 1
            # DEBUG
            elif event.key == pygame.K_s:
                if event.type == pygame.KEYDOWN:
                    self.power_meter.set_dirty()
                    if self.shot_power < 101:
                        self.shot_power += 1

            elif event.key == pygame.K_x:
                if event.type == pygame.KEYDOWN:
                    self.power_meter.set_dirty()
                    if self.shot_power > 1:
                        self.shot_power -= 1

    def fire(self):
        ''' Launch a ball '''
        self.sound.play()
        x = 190
        y = -210
        ball = Ball(self.shot_power, self.shot_rads, x, y, self.space)
        if len(self.balls) == 5:
            first_ball = self.balls.sprites()[0]
            self.space.remove(first_ball.shape, first_ball.shape.body)
            self.balls.remove(first_ball)
            self.all_sprites.remove(first_ball)
        self.balls.add(ball)
        self.all_sprites.add(ball, layer=BALL_LAYER)

    def load_music(self):
        """Load the music"""
        pygame.mixer.music.load(os.path.join(BASE_DIR, 'projectilegravity/resources/sounds/angry-birds.ogg'))
        pygame.mixer.music.play(-1)

    def post_solve_ball_block(self, arbiter, space, _):
        """Collision between ball and dynamic block"""
        a, b = arbiter.shapes
        ball_body = a.body
        for ball in self.balls:
            if ball_body == ball.body:
                space.remove(ball.shape, ball.shape.body)
                self.balls.remove(ball)

    def post_solve_ball_platform(self, arbiter, space, _):
        """Collision between ball and dynamic block"""
        a, b = arbiter.shapes
        ball_body = a.body
        platform_body = b.body
        for platform in self.platforms:
            if platform_body == platform.body:
                platform.set_dirty()

    def post_solve_block_platform(self, arbiter, space, _):
        """Collision between ball and dynamic block"""
        a, b = arbiter.shapes
        block_body = a.body
        platform_body = b.body
        for platform in self.platforms:
            if platform_body == platform.body:
                platform.set_dirty()

    def draw_level_cleared(self):
        """Draw self.level cleared"""

        # If there are green and red blocks
        if self.green_blocks_num and self.red_blocks_num:

            # Check to see if there are red blocks still in play
            # If there are no red blocks on the screen then we fail
            if not self.red_blocks:
                # Failed
                self.game_state = GAME_STATE_FAILED_LEVEL
                self.menu_button = 2
                self.failed_menu.set_dirty()
                self.failed_retry_button.update(on=True)
                self.failed_menu_button.update(on=False)
                self.all_sprites.add(self.failed_menu, layer=UI_LAYER)
                self.all_sprites.add(self.failed_retry_button, layer=UI_LAYER)
                self.all_sprites.add(self.failed_menu_button, layer=UI_LAYER)
            else:

                # Check and see if any red blocks are still in play
                all_bellow = True
                for block in self.red_blocks:
                    if block.shape.body.position.y > self.lowest_platform_height:
                        all_bellow = False

                if all_bellow:
                    self.game_state = GAME_STATE_FAILED_LEVEL
                    self.menu_button = 2
                    self.failed_menu.set_dirty()
                    self.failed_retry_button.update(on=True)
                    self.failed_menu_button.update(on=False)
                    self.all_sprites.add(self.failed_menu, layer=UI_LAYER)
                    self.all_sprites.add(self.failed_retry_button, layer=UI_LAYER)
                    self.all_sprites.add(self.failed_menu_button, layer=UI_LAYER)

                # There are still red blocks in play
                # Check the green blocks
                else:

                    # If there are no green blocks left on the screen
                    # we've completed the level
                    if not self.green_blocks:
                        self.game_state = GAME_STATE_COMPLETED_LEVEL
                        self.menu_button = 2
                        self.completed_menu.set_dirty()
                        self.completed_continue_button.update(on=True)
                        self.completed_menu_button.update(on=False)
                        self.all_sprites.add(self.completed_menu, layer=UI_LAYER)
                        self.all_sprites.add(self.completed_continue_button, layer=UI_LAYER)
                        self.all_sprites.add(self.completed_menu_button, layer=UI_LAYER)

                    # Check and see if all of the blocks are on the ground
                    # if they are all on the ground then we've completed the level
                    else:
                        all_bellow = True
                        for block in self.green_blocks:
                            if block.shape.body.position.y > self.lowest_platform_height:
                                all_bellow = False

                        if all_bellow:
                            self.game_state = GAME_STATE_COMPLETED_LEVEL
                            self.menu_button = 2
                            self.completed_menu.set_dirty()
                            self.completed_continue_button.update(on=True)
                            self.completed_menu_button.update(on=False)
                            self.all_sprites.add(self.completed_menu, layer=UI_LAYER)
                            self.all_sprites.add(self.completed_continue_button, layer=UI_LAYER)
                            self.all_sprites.add(self.completed_menu_button, layer=UI_LAYER)

                        else:
                            # We're still playing
                            pass

        # Else there are just green blocks
        elif self.green_blocks_num:

            # If there are no green blocks left on the screen
            if not self.green_blocks:
                self.game_state = GAME_STATE_COMPLETED_LEVEL
                self.menu_button = 2
                self.completed_menu.set_dirty()
                self.completed_continue_button.update(on=True)
                self.completed_menu_button.update(on=False)
                self.all_sprites.add(self.completed_menu, layer=UI_LAYER)
                self.all_sprites.add(self.completed_continue_button, layer=UI_LAYER)
                self.all_sprites.add(self.completed_menu_button, layer=UI_LAYER)

            # Check and see if all of the blocks are on the ground
            else:
                all_bellow = True
                for block in self.green_blocks:
                    if block.shape.body.position.y > self.lowest_platform_height:
                        all_bellow = False

                if all_bellow:
                    self.game_state = GAME_STATE_COMPLETED_LEVEL
                    self.menu_button = 2
                    self.completed_menu.set_dirty()
                    self.completed_continue_button.update(on=True)
                    self.completed_menu_button.update(on=False)
                    self.all_sprites.add(self.completed_menu, layer=UI_LAYER)
                    self.all_sprites.add(self.completed_continue_button, layer=UI_LAYER)
                    self.all_sprites.add(self.completed_menu_button, layer=UI_LAYER)


    def draw_level_failed(self):
        """Draw self.level failed"""

        failed = self.bold_font3.render("Level Failed", 1, WHITE)
        if self.red_blocks_num and not self.red_blocks:
            self.game_state = GAME_STATE_FAILED_LEVEL
            rect = pygame.Rect(300, 0, 600, 800)
            pygame.draw.rect(self.screen, BLACK, rect)
            self.screen.blit(failed, (450, 90))
            # self.screen.blit(self.replay_button, (520, 460))

    def restart(self):
        """Delete all objects of the self.level"""
        for ball in self.balls:
            self.space.remove(ball.shape, ball.shape.body)
            self.balls.remove(ball)
            self.all_sprites.remove(ball)
        for block in self.green_blocks:
            self.space.remove(block.shape, block.shape.body)
            self.green_blocks.remove(block)
            self.all_sprites.remove(block)
        for block in self.red_blocks:
            self.space.remove(block.shape, block.shape.body)
            self.red_blocks.remove(block)
            self.all_sprites.remove(block)

        for block in self.green_blocks_to_create:
            self.green_blocks.add(GreenBlock(block, self.space))
        for block in self.red_blocks_to_create:
            self.red_blocks.add(RedBlock(block, self.space))
        self.all_sprites.add(self.green_blocks, layer=BLOCK_LAYER)
        self.all_sprites.add(self.red_blocks, layer=BLOCK_LAYER)

        self.game_state = GAME_STATE_RUNNING
        self.all_sprites.remove(self.failed_menu)
        self.all_sprites.remove(self.failed_menu_button)
        self.all_sprites.remove(self.failed_retry_button)


    def load_level(self):
        ''' Load the level '''
        self.running = True

        while self.running:

            if self.frame == CLOCK_TICK_RATE:
                self.frame = 0
                self.timeout_counter += 1
            else:
                self.frame += 1

            # TIMEOUT
            # If the we reach the time out limit we exit the game
            if self.timeout_counter == self.timeout_value:
                self.running = False
                return 'TIMEOUT'

            events = pygame.event.get()
            next_game_to_load = self.handle_input_events(events)
            if next_game_to_load:
                return next_game_to_load

            #####################
            # Update physics
            #####################
            d_t = 1.0/CLOCK_TICK_RATE/2.
            for x in range(2):
                self.space.step(d_t) # make two updates per frame for better stability


            #####################
            # REMOVAL
            #####################
            balls_to_remove = []
            green_blocks_to_remove = []
            red_blocks_to_remove = []

            for ball in self.balls:
                # if ball.shape.body.position.y < (-(PHYSICAL_SCREEN.height/2) + self.floor_height):
                if self.is_off_screen(ball.shape.body.position.x, ball.shape.body.position.y, self.gravity):
                    balls_to_remove.append(ball)

            for block in self.green_blocks:
                # if block.shape.body.position.y < (-(PHYSICAL_SCREEN.height/2) + self.floor_height):
                if self.is_off_screen(block.shape.body.position.x, block.shape.body.position.y, self.gravity):
                    green_blocks_to_remove.append(block)

            for block in self.red_blocks:
                # if block.shape.body.position.y < (-(PHYSICAL_SCREEN.height/2) + self.floor_height):
                if self.is_off_screen(block.shape.body.position.x, block.shape.body.position.y, self.gravity):
                    red_blocks_to_remove.append(block)

            # Remove balls and blocks
            for ball in balls_to_remove:
                self.space.remove(ball.shape, ball.shape.body)
                self.balls.remove(ball)
                self.all_sprites.remove(ball)
            for block in green_blocks_to_remove:
                self.space.remove(block.shape, block.shape.body)
                self.green_blocks.remove(block)
                self.all_sprites.remove(block)
            for block in red_blocks_to_remove:
                self.space.remove(block.shape, block.shape.body)
                self.red_blocks.remove(block)
                self.all_sprites.remove(block)

            #####################
            # DRAW
            #####################
            # for line in [self.ground]:
            #     body = line.body
            #     pv1 = body.position + line.a.rotated(body.angle)
            #     pv2 = body.position + line.b.rotated(body.angle)
            #     p1 = to_pygame(pv1)
            #     p2 = to_pygame(pv2)
            #     pygame.draw.lines(self.screen, (150, 150, 150), False, [p1, p2])

            dirty_rects = []

            # Update Platforms
            self.platforms.update()

            # Update Cannon
            self.cannon.update(self.shot_angle)

            # Update Balls
            self.balls.update()

            # Update Green Blocks
            self.green_blocks.update()

            # Update Red Blocks
            self.red_blocks.update()

            # Update UI Elements
            self.green_block_text.update(len(self.green_blocks))

            if self.red_blocks_num:
                self.red_block_text.update(len(self.red_blocks))

            self.angle_text.update(self.shot_angle)

            self.power_meter.update(self.shot_power)

            # Pause option
            # if self.game_state == GAME_STATE_PAUSED:
            #     self.screen.blit(self.play_button, (500, 200))
            #     self.screen.blit(self.replay_button, (500, 300))

            if self.game_state == GAME_STATE_RUNNING:
                self.draw_level_cleared()
                # self.draw_level_failed()

            dirty_rects = self.all_sprites.draw(self.screen)
            pygame.display.update(dirty_rects)

            self.clock.tick(CLOCK_TICK_RATE)

            # pygame.display.set_caption("fps: " + str(clock.get_fps()))
            pygame.display.set_caption(self.name)


class Blackhole1(Level):

    ''' Blackhole level 1 '''

    name = 'Blackhole Level 1'
    gravity = (0.0, BASE_GRAVITY * 10)
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/blackhole.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = None

    # Platforms
    platforms_to_create = [
        # Floor under cannon
        (120, -370),
        (170, -370),
        (220, -370),
        (270, -370),
        (320, -370),
        (370, -370),
        (420, -370),
        (470, -370),
        # Platforms
        (800, -100),
    ]
    walls_to_create = []
    green_blocks_to_create = [
        (800, -49),
    ]
    green_blocks_num = len(green_blocks_to_create)

class Pluto1(Level):

    ''' Pluto level 1 '''

    name = 'Pluto Level 1'
    gravity = (0.0, BASE_GRAVITY * 0.071) # 0.071 g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/pluto.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Blackhole1

    has_ground = True

    # Platforms
    platforms_to_create = [
        (1400, 0),
    ]
    walls_to_create = []
    green_blocks_to_create = [
        (1400, 51),
        (1375, 51*2),
        (1425, 51*2),
        (1350, 51*3),
        (1400, 51*3),
        (1450, 51*3),
        (1375, 51*4),
        (1425, 51*4),
        (1400, 51*5),
    ]
    green_blocks_num = len(green_blocks_to_create)

class Neptune1(Level):

    ''' Neptune level 1 '''

    name = 'Neptune Level 1'
    gravity = (0.0, BASE_GRAVITY * 1.14) # 11.15 m/s2 (or 1.14 g)
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/neptune.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Pluto1

    # Platforms
    platforms_to_create = [
        # Floor under cannon
        (120, -370),
        (170, -370),
        (220, -370),
        (270, -370),
        (320, -370),
        (370, -370),
        (420, -370),
        (470, -370),
        # Platforms
        (1400, 0),
    ]
    walls_to_create = []
    green_blocks_to_create = [
        (1400, 51),
        (1375, 51*2),
        (1425, 51*2),
        (1350, 51*3),
        (1400, 51*3),
        (1450, 51*3),
        (1375, 51*4),
        (1425, 51*4),
        (1400, 51*5),
    ]
    green_blocks_num = len(green_blocks_to_create)

class Uranus1(Level):

    ''' Uranus level 1 '''

    name = 'Uranus Level 1'
    gravity = (0.0, BASE_GRAVITY * 0.886) # 8.69 m/s2, or 0.886 g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/uranus.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Neptune1

    # Platforms
    platforms_to_create = [
        # Floor under cannon
        (120, -370),
        (170, -370),
        (220, -370),
        (270, -370),
        (320, -370),
        (370, -370),
        (420, -370),
        (470, -370),
        # Platforms
        (1200, 0),
        (1400, 0),
        (1600, 0),
    ]

    walls_to_create = []

    green_blocks_to_create = [
        (1200, 51*3),
        (1400, 51*3),
        (1600, 51*3),
    ]
    green_blocks_num = len(green_blocks_to_create)

    red_blocks_to_create = [
        (1200, 51),
        (1200, 51*2),
        (1400, 51),
        (1400, 51*2),
        (1600, 51),
        (1600, 51*2),
    ]
    red_blocks_num = len(red_blocks_to_create)

class Saturn1(Level):

    ''' Saturn level 1 '''

    name = 'Saturn Level 1'
    gravity = (0.0, BASE_GRAVITY * 1.065) # 10.44 m/s2 (or 1.065 g)
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/saturn.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Uranus1

    # Platforms
    platforms_to_create = [
        # Floor under cannon
        (120, -370),
        (170, -370),
        (220, -370),
        (270, -370),
        (320, -370),
        (370, -370),
        (420, -370),
        (470, -370),
        # Platforms
        (1200, 0),
        (1400, 0),
        (1600, 0),
    ]

    walls_to_create = []

    green_blocks_to_create = [
        (1400, 51),
    ]
    green_blocks_num = len(green_blocks_to_create)

    red_blocks_to_create = [
        (1200, 51),
        (1600, 51),
    ]
    red_blocks_num = len(red_blocks_to_create)

class Jupiter1(Level):

    ''' Jupiter level 1 '''

    name = 'Jupiter Level 1'
    gravity = (0.0, BASE_GRAVITY * 2.528) # 24.79 m/s, or 2.528 g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/jupiter.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Saturn1

    # Platforms
    platforms_to_create = [
        # Floor under cannon
        (120, -370),
        (170, -370),
        (220, -370),
        (270, -370),
        (320, -370),
        (370, -370),
        (420, -370),
        (470, -370),
        # Platforms
        (1200, 0),
        (1400, 0),
        (1600, 0),
    ]
    walls_to_create = []
    green_blocks_to_create = [
        (1200, 51),
        (1200, 51*2),
        (1200, 51*3),
        (1400, 51),
        (1400, 51*2),
        (1400, 51*3),
        (1600, 51),
        (1600, 51*2),
        (1600, 51*3),
    ]
    green_blocks_num = len(green_blocks_to_create)

class Mars1(Level):

    ''' Mars level 1 '''

    name = 'Mars Level 1'
    gravity = (0.0, BASE_GRAVITY * 0.38)
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/mars.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Jupiter1

    has_ground = True

    # Platforms
    platforms_to_create = [
        (1400, -100),
        (1400, 100),
        (1400, 300),
    ]

    walls_to_create = []

    green_blocks_to_create = [
        (1400, 151),
    ]
    green_blocks_num = len(green_blocks_to_create)

    red_blocks_to_create = [
        (1400, -49),
        (1400, 351),
    ]
    red_blocks_num = len(red_blocks_to_create)

class Moon1(Level):

    ''' Moon level 1 '''

    name = 'Moon Level 1'
    gravity = (0.0, BASE_GRAVITY * 0.165) # 1.62 m/s2 , or 0.1654 g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/moon.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Mars1

    has_ground = True

    # Platforms
    platforms_to_create = [
        # Floor under cannon
        (120, -370),
        (170, -370),
        (220, -370),
        (270, -370),
        (320, -370),
        (370, -370),
        (420, -370),
        (470, -370),
        # Platforms
        (1450, 0),
    ]
    walls_to_create = []
    green_blocks_to_create = [
        (1450, 51),
        (1450, 51*2),
        (1450, 51*3),
        (1450, 51*4)
    ]
    green_blocks_num = len(green_blocks_to_create)

class ISS1(Level):

    ''' Space level 1 '''

    name = 'Space Level 1'
    gravity = (0.0, BASE_GRAVITY * 0.0000001) # 0g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/space.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Moon1

    # Platforms
    platforms_to_create = [
        # Floor under cannon
        (120, -370),
        (170, -370),
        (220, -370),
        (270, -370),
        (320, -370),
        (370, -370),
        (420, -370),
        (470, -370),
        # Platforms
    ]
    walls_to_create = []

    green_blocks_to_create = [
        (1400, 300),
        (1200, 100),
        (1600, 100),
        (1400, -100),
    ]
    green_blocks_num = len(green_blocks_to_create)

class Earth1(Level):

    ''' Earth level 1 '''

    name = 'Earth Level 1'
    gravity = (0.0, BASE_GRAVITY) # 9.80 m/s2, or 1.0 g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/earth.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = ISS1

    has_ground = True

    # Platforms
    platforms_to_create = [
        (1400, -100),
        (1400, 100),
        (1400, 300),
    ]

    walls_to_create = []

    green_blocks_to_create = [
        (1400, -49),
        (1400, 151),
        (1400, 351),
    ]
    green_blocks_num = len(green_blocks_to_create)

class Venus1(Level):

    ''' Venus level 1 '''
    name = 'Venus Level 1'
    gravity = (0.0, BASE_GRAVITY * 0.904) # 8.87 m/s2, or 0.904 g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/venus.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Earth1

    has_ground = True

    # Platforms
    platforms_to_create = [
        (1200, 0),
        (1400, 0),
        (1600, 0),
    ]

    walls_to_create = []

    green_blocks_to_create = [
        (1200, 51),
        (1400, 51),
        (1600, 51),
    ]
    green_blocks_num = len(green_blocks_to_create)

class Mercury1(Level):

    ''' Mercury level 1 '''
    name = 'Mercury Level 1'
    gravity = (0.0, BASE_GRAVITY * 0.38) # 3.7 m/s2, or 0.38 g
    background = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planet_backgrounds/mercury.png')).convert_alpha()
    level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/level_icons/level1@3x.png')).convert_alpha()
    next_level = Venus1

    has_ground = True

    # Platforms
    platforms_to_create = [
        (1400, 0),
    ]
    walls_to_create = []

    green_blocks_to_create = [
        (1400, 51),
    ]
    green_blocks_num = len(green_blocks_to_create)
