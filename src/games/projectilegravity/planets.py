import os
import math
import pygame
import pymunk as pm
from constants import WHITE
from constants import ALIGN_CENTER_H, ALIGN_CENTER_V
from .levels import Mercury1, Venus1, Earth1, ISS1, Moon1, Mars1, Jupiter1, Saturn1, Uranus1, Neptune1, Pluto1, Blackhole1

from global_sprites import Text, Asset

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# pygame.init()

all_planet_infos = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/menus/planet_descriptions.png')).convert_alpha()


class Planet():
    '''
    This is the main class the defines a game level

    Subclass to create a new level
    '''

    space = pm.Space()
    # screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN | pygame.NOFRAME)
    screen = pygame.display.set_mode([1920, 1080])

    shot_angle = 45
    shot_power = 50
    shot_rads = math.radians(shot_angle)

    balls = []

    name = ''
    background = None
    level_image = None
    sub_planets = []
    next_level = None
    text_sprite = None
    image_sprite = None
    info_sprite = None
    sprites = None

    def setup(self):
        self.sprites = pygame.sprite.Group()
        if self.text_sprite:
            self.sprites.add(self.text_sprite)
        if self.image_sprite:
            self.sprites.add(self.image_sprite)
        if self.info_sprite:
            self.sprites.add(self.info_sprite)

    def set_sprites_dirty(self):
        ''' Set all the sprites to dirty'''
        for sprite in self.sprites:
            sprite.set_dirty()


class Blackhole(Planet):
    ''' Planet Blackhole '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Blackhole'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/blackhole.png')).convert_alpha()
        self.levels = [Blackhole1]
        self.setup()


class Pluto(Planet):
    ''' Planet Pluto '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Pluto'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/pluto.png')).convert_alpha()
        self.levels = [Pluto1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 9250, 800, 925), create_dirty=False)
        self.setup()


class Neptune(Planet):
    ''' Planet Neptune '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Neptune'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/neptune.png')).convert_alpha()
        self.levels = [Neptune1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 8325, 800, 925), create_dirty=False)
        self.setup()


class Uranus(Planet):
    ''' Planet Uranus '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Uranus'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/uranus.png')).convert_alpha()
        self.levels = [Uranus1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 7400, 800, 925), create_dirty=False)
        self.setup()


class Saturn(Planet):
    ''' Planet Saturn '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Saturn'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/saturn.png')).convert_alpha()
        self.levels = [Saturn1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 6475, 800, 925), create_dirty=False)
        self.setup()


class Jupiter(Planet):
    ''' Planet Jupiter '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Jupiter'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/jupiter.png')).convert_alpha()
        self.levels = [Jupiter1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 5550, 800, 925), create_dirty=False)
        self.setup()


class Mars(Planet):
    ''' Planet Mars '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Mars'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/mars.png')).convert_alpha()
        self.levels = [Mars1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 4625, 800, 925), create_dirty=False)
        self.setup()


class Moon(Planet):
    ''' Planet Moon '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Moon'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/moon.png')).convert_alpha()
        self.levels = [Moon1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 2775, 800, 925), create_dirty=False)
        self.setup()


class ISS(Planet):
    ''' Planet Space '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Space'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/space.png')).convert_alpha()
        self.levels = [ISS1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 3700, 800, 925), create_dirty=False)
        self.setup()


class Earth(Planet):
    ''' Planet Earth '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Earth'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/earth.png')).convert_alpha()
        self.levels = [Earth1]
        self.sub_planets = [ISS, Moon]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 1850, 800, 925), create_dirty=False)
        self.setup()


class Venus(Planet):
    ''' Planet Venus '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Venus'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/venus.png')).convert_alpha()
        self.levels = [Venus1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 925, 800, 925), create_dirty=False)
        self.setup()


class Mercury(Planet):
    ''' Planet Mercury '''

    def __init__(self):
        self.bold_font3 = pygame.font.SysFont("Minecraftia", 72)
        self.name = 'Mercury'
        self.level_image = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/planets/mercury.png')).convert_alpha()
        self.levels = [Mercury1]

        self.text_sprite = Text((550, 250), WHITE, self.bold_font3, self.name, align_h=ALIGN_CENTER_H)
        self.image_sprite = Asset((550, 540), self.level_image, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)
        self.info_sprite = Asset((1070, 50), all_planet_infos, rect=pygame.Rect(0, 0, 800, 925), create_dirty=False)
        self.setup()
