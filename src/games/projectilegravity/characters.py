import pymunk as pm
from pymunk import Vec2d


class Ball():
    ''' Ball class '''
    def __init__(self, distance, angle, x, y, space):
        self.life = 20
        mass = 5
        radius = 24
        inertia = pm.moment_for_circle(mass, 0, radius, (0, 0))
        body = pm.Body(mass, inertia)
        body.position = x, y
        power = distance * 100
        impulse = power * Vec2d(1, 0)
        # angle = -angle
        body.apply_impulse_at_local_point(impulse.rotated(angle))
        # shape = pm.Circle(body, radius, (0, 0))
        shape = pm.Poly.create_box(body, (50, 50))
        shape.elasticity = 0.5
        shape.friction = .5
        shape.collision_type = 0
        space.add(body, shape)
        self.body = body
        self.shape = shape
