import os
import math
import pygame
import pymunk as pm
from pymunk import Vec2d
from constants import PHYSICAL_SCREEN
from constants import ALIGN_LEFT, ALIGN_CENTER_H, ALIGN_RIGHT, ALIGN_TOP, ALIGN_CENTER_V, ALIGN_BOTTOM


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def to_pygame(pos):
    """Convert pymunk to pygame coordinates"""
    return int(pos.x), int(-pos.y+600)

class Ball(pygame.sprite.DirtySprite):
    ''' Ball sprite '''

    def __init__(self, distance, angle, x, y, space):

        super(Ball, self).__init__()

        mass = 5
        radius = 24
        inertia = pm.moment_for_circle(mass, 0, radius, (0, 0))
        body = pm.Body(mass, inertia)
        body.position = x + 10, y + 5
        power = distance * 140
        impulse = power * Vec2d(1, 0)
        # angle = -angle
        body.apply_impulse_at_local_point(impulse.rotated(angle))
        # shape = pm.Circle(body, radius, (0, 0))
        shape = pm.Poly.create_box(body, (50, 50))
        shape.elasticity = 0
        shape.friction = .5
        shape.collision_type = 0
        space.add(body, shape)
        self.dirty = 1
        self.body = body
        self.shape = shape
        self.image = pygame.image.load(os.path.join(BASE_DIR, "projectilegravity/resources/images/objects/ball.png")).convert_alpha()
        self.rect = pygame.Rect((x, y), (50, 50))

    def update(self):
        ''' Update the position of the ball '''
        self.dirty = 1
        p = to_pygame(self.shape.body.position)
        x, y = p
        x -= 22
        y -= 20
        self.rect = ((x, y), (50, 50))


class GreenBlock(pygame.sprite.DirtySprite):

    ''' A polygon that holds a hitable block'''
    def __init__(self, pos, space, length=50, height=50, mass=5.0):

        super(GreenBlock, self).__init__()

        moment = 1000
        body = pm.Body(mass, moment)
        body.position = Vec2d(pos)
        shape = pm.Poly.create_box(body, (length, height))
        shape.color = (0, 0, 255)
        shape.friction = 0.5
        shape.collision_type = 1
        space.add(body, shape)
        self.dirty = 1
        self.size = (length, height)
        self.body = body
        self.shape = shape
        self.green_image = pygame.image.load(os.path.join(BASE_DIR, "projectilegravity/resources/images/objects/green-block.png")).convert_alpha()
        self.image = self.green_image
        self.rect = pygame.Rect(pos, self.size)

    def update(self):
        '''This is the method that's being called when 'green_blocks.update()' is called.'''

        if self.body.velocity:
            self.dirty = 1

            poly = self.shape
            position = poly.body.position
            position = Vec2d(to_pygame(position))
            angle_degrees = math.degrees(poly.body.angle) + 180
            self.image = pygame.transform.rotate(self.green_image, angle_degrees)

            offset = Vec2d(self.image.get_size()) / 2.
            position = position - offset
            self.rect = pygame.Rect(position, self.size)


class RedBlock(pygame.sprite.DirtySprite):

    ''' A polygon that holds a hitable block'''
    def __init__(self, pos, space, length=50, height=50, mass=5.0):

        super(RedBlock, self).__init__()

        moment = 1000
        body = pm.Body(mass, moment)
        body.position = Vec2d(pos)
        shape = pm.Poly.create_box(body, (length, height))
        shape.color = (0, 0, 255)
        shape.friction = 0.5
        shape.collision_type = 1
        space.add(body, shape)
        self.dirty = 1
        self.size = (length, height)
        self.body = body
        self.shape = shape
        self.red_image = pygame.image.load(os.path.join(BASE_DIR, "projectilegravity/resources/images/objects/red-block.png")).convert_alpha()
        self.image = self.red_image
        self.rect = pygame.Rect(pos, self.size)

    def update(self):
        '''This is the method that's being called when 'green_blocks.update()' is called.'''

        if self.body.velocity:
            self.dirty = 1

            poly = self.shape
            position = poly.body.position
            position = Vec2d(to_pygame(position))
            angle_degrees = math.degrees(poly.body.angle) + 180
            self.image = pygame.transform.rotate(self.red_image, angle_degrees)

            offset = Vec2d(self.image.get_size()) / 2.
            position = position - offset
            self.rect = pygame.Rect(position, self.size)


class Platform(pygame.sprite.DirtySprite):
    ''' A polygon that holds a platform block '''

    def __init__(self, pos, space, length=50, height=50,):

        super(Platform, self).__init__()

        self.dirty = 1
        self.position = pos
        self.size = (length, height)
        self.body = pm.Body(body_type=pm.Body.STATIC)
        self.body.position = Vec2d(pos)
        self.shape = pm.Poly.create_box(self.body, self.size)
        # self.shape.color = (0, 0, 255)
        self.shape.friction = 0.5
        self.shape.collision_type = 2
        space.add(self.body, self.shape)

        self.image = pygame.image.load(os.path.join(BASE_DIR, "projectilegravity/resources/images/objects/static-block.png")).convert_alpha()
        offset = Vec2d(self.image.get_size()) / 2
        # screen.blit(self.image, Vec2d(to_pygame(self.shape.body.position)) - offset)
        self.rect = pygame.Rect(Vec2d(to_pygame(self.shape.body.position)) - offset, self.size)

    def set_dirty(self):
        self.dirty = 1

    def update(self):
        """Draw platforms and walls"""
        offset = Vec2d(self.image.get_size()) / 2
        # screen.blit(self.image, Vec2d(to_pygame(self.shape.body.position)) - offset)
        self.rect = pygame.Rect(Vec2d(to_pygame(self.shape.body.position)) - offset, self.size)


class Cannon(pygame.sprite.DirtySprite):
    ''' Cannon Sprite '''

    def __init__(self, floor_height, starting_angle):

        super(Cannon, self).__init__()

        self.floor_height = floor_height
        self.position = (120, PHYSICAL_SCREEN.height - 205 - self.floor_height)
        self.dirty = 1
        self.sprite_map = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/cannon/cannon.png')).convert_alpha()
        self.image = self.get_cannon(starting_angle)
        self.rect = pygame.Rect(self.position, (270, 270))

    def set_dirty(self):
        ''' set the sprite to dirty '''
        self.dirty = 1

    def get_cannon(self, angle):
        '''
        input: angle of shot
        output: corresponding cannon image
        '''

        if angle >= 0 and angle <= 8:
            location = 270*12
        elif angle > 8 and angle <= 15:
            location = 270*11
        elif angle > 15 and angle <= 22:
            location = 270*10
        elif angle > 22 and angle <= 29:
            location = 270*9
        elif angle > 29 and angle <= 36:
            location = 270*8
        elif angle > 36 and angle <= 43:
            location = 270*7
        elif angle > 43 and angle <= 50:
            location = 270*6
        elif angle > 50 and angle <= 57:
            location = 270*5
        elif angle > 57 and angle <= 65:
            location = 270*4
        elif angle > 64 and angle <= 71:
            location = 270*3
        elif angle > 71 and angle <= 78:
            location = 270*2
        elif angle > 78 and angle <= 85:
            location = 270
        elif angle > 85 and angle <= 90:
            location = 0

        rect = pygame.Rect(location, 0, 270, 270)

        return self.sprite_map.subsurface(rect).copy()

    def update(self, angle):
        ''' Update cannon image '''
        self.image = self.get_cannon(angle)

class PowerMeter(pygame.sprite.DirtySprite):
    ''' Power Meter Sprite '''

    def __init__(self, floor_height, starting_power):

        super(PowerMeter, self).__init__()

        self.floor_height = floor_height
        self.position = (30, 277)
        self.dirty = 1
        self.sprite_map = pygame.image.load(os.path.join(BASE_DIR, 'projectilegravity/resources/images/power_meter/power_meter.png')).convert_alpha()
        self.image = self.get_power_meter(starting_power)
        self.rect = pygame.Rect(self.position, (80, 560))

    def set_dirty(self):
        ''' set the sprite to dirty '''
        self.dirty = 1

    def get_power_meter(self, power):
        '''
        input: shot power
        output: sliced sprite map for power meter
        '''
        if power == 100:
            location = (80 * 100)
        else:
            location = (80 * power)

        rect = pygame.Rect(location, 0, 80, 560)

        return self.sprite_map.subsurface(rect).copy()

    def update(self, power):
        ''' Update the power meter '''
        self.image = self.get_power_meter(power)

class Button(pygame.sprite.DirtySprite):

    def __init__(self, pos, sprite_map, off_rect, on_rect, on=False, align_h=ALIGN_LEFT, align_v=ALIGN_TOP):

        super(Button, self).__init__()

        self.align_h = align_h
        self.align_v = align_v
        self.position = self.get_position(pos)
        self.dirty = 0
        self.off_image = sprite_map.subsurface(off_rect).copy()
        self.on_image = sprite_map.subsurface(on_rect).copy()
        self.image = self.off_image
        if on:
            self.image = self.on_image

        self.rect = pygame.Rect(self.position, (self.image.get_rect().width, self.image.get_rect().height))

    def set_dirty(self):
        ''' Set image to update '''
        self.dirty = 1

    def get_position(self, pos):
        ''' get new position of image '''
        x = 0
        y = 0
        if self.align_h == ALIGN_LEFT:
            x = pos[0]
        elif self.align_h == ALIGN_CENTER_H:
            x = pos[0] - self.image.get_rect().width / 2
        elif self.align_h == ALIGN_RIGHT:
            x = pos[0] - self.image.get_rect().width

        if self.align_v == ALIGN_TOP:
            y = pos[1]
        elif self.align_v == ALIGN_CENTER_V:
            y = pos[1] - self.image.get_rect().height / 2
        elif self.align_v == ALIGN_BOTTOM:
            y = pos[1] - self.image.get_rect().height

        return (x, y)

    def update(self, pos=None, on=True):
        ''' Update image '''
        if pos:
            self.position = self.get_position(pos)

        if on:
            self.image = self.on_image
        else:
            self.image = self.off_image

        self.set_dirty()
