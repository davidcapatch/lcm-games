#include <Wire.h>
#include "Adafruit_MCP23017.h"

// Adafruit_MCP23017
Adafruit_MCP23017 mcp0;
Adafruit_MCP23017 mcp1;
Adafruit_MCP23017 mcp2;
Adafruit_MCP23017 mcp3;
#define addr0 0x20
#define addr1 0x21
#define addr2 0x22
#define addr3 0x23
#define addr4 0x24


//----------------------------
// PANEL ROW 3
//----------------------------
//
const int P3BINP2B1 = 2;
const int P3BINP2B3 = 3;
const int P3CINP1A1 = 18;
const int P3CINP1A3 = 19;
const int P3CINP1B1 = 20;
const int P3CINP1B3 = 21;
// MEGA 2560 Digital Pins 22-52
const int P3DINP1A = 22;
const int P3DLED1A = 23;
const int P3DINP1B = 24;
const int P3DLED1B = 25;
const int P3DINP1C = 26;
const int P3DLED1C = 27;
const int P3DINP2A = 28;
const int P3DLED2A = 29;
const int P3DINP2B = 30;
const int P3DLED2B = 31;
const int P3DINP2C = 32;
const int P3DLED2C = 33;
const int P3DINP3A = 34;
const int P3DLED3A = 35;
const int P3DINP3B = 36;
const int P3DLED3B = 37;
const int P3DINP3C = 38;
const int P3DLED3C = 39;
//
const int P3FINP1A = 40;
const int P3FLED1A = 41;


//----------------------------
// PANEL ROW 3
// Storage for button LEDs
//----------------------------

int P3DLED1A_state = 0;
int P3DLED1B_state = 0;
int P3DLED1C_state = 0;
int P3DLED2A_state = 0;
int P3DLED2B_state = 0;
int P3DLED2C_state = 0;
int P3DLED3A_state = 0;
int P3DLED3B_state = 0;
int P3DLED3C_state = 0;
//
int P3FLED1A_state = 0;
