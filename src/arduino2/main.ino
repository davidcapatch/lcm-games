#include <Bounce2.h>

#include "Rotary.h"
#include "include/constants.h"


// Time tracking
// How long to wait
int interval = 1000;
// Time since last event
unsigned long previousMillis = 0;

#define BUTTON_INTERVAL 25


// Define rotary encoders
Rotary r1 = Rotary(P3BINP2B1, P3BINP2B3);
Rotary r2 = Rotary(P3CINP1A1, P3CINP1A3);
Rotary r3 = Rotary(P3CINP1B1, P3CINP1B3);
// Define buttons
Bounce P3DINP1A_b = Bounce();
Bounce P3DINP1B_b = Bounce();
Bounce P3DINP1C_b = Bounce();
Bounce P3DINP2A_b = Bounce();
Bounce P3DINP2B_b = Bounce();
Bounce P3DINP2C_b = Bounce();
Bounce P3DINP3A_b = Bounce();
Bounce P3DINP3B_b = Bounce();
Bounce P3DINP3C_b = Bounce();
//
Bounce P3FINP1A_b = Bounce();

// Define interrupt functions
void check1()
{
    unsigned char result = r1.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1009,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1009,0");
    }
}

void check2()
{
    unsigned char result = r2.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1011,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1011,0");
    }
}

void check3()
{
    unsigned char result = r3.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1013,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1013,0");
    }
}

void setup()
{
    //start serial connection
    Serial.begin(9600);

    // Attach interrupts to rotary encoders
    attachInterrupt(digitalPinToInterrupt(P3BINP2B1), check1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3BINP2B3), check1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3CINP1A1), check2, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3CINP1A3), check2, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3CINP1B1), check3, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3CINP1B3), check3, CHANGE);

    P3DINP1A_b.attach(P3DINP1A, INPUT_PULLUP);
    P3DINP1A_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED1A, OUTPUT);
    P3DINP1B_b.attach(P3DINP1B, INPUT_PULLUP);
    P3DINP1B_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED1B, OUTPUT);
    P3DINP1C_b.attach(P3DINP1C, INPUT_PULLUP);
    P3DINP1C_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED1C, OUTPUT);
    P3DINP2A_b.attach(P3DINP2A, INPUT_PULLUP);
    P3DINP2A_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED2A, OUTPUT);
    P3DINP2B_b.attach(P3DINP2B, INPUT_PULLUP);
    P3DINP2B_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED2B, OUTPUT);
    P3DINP2C_b.attach(P3DINP2C, INPUT_PULLUP);
    P3DINP2C_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED2C, OUTPUT);
    P3DINP3A_b.attach(P3DINP3A, INPUT_PULLUP);
    P3DINP3A_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED3A, OUTPUT);
    P3DINP3B_b.attach(P3DINP3B, INPUT_PULLUP);
    P3DINP3B_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED3B, OUTPUT);
    P3DINP3C_b.attach(P3DINP3C, INPUT_PULLUP);
    P3DINP3C_b.interval(BUTTON_INTERVAL);
    pinMode(P3DLED3C, OUTPUT);
    P3FINP1A_b.attach(P3FINP1A, INPUT_PULLUP);
    P3FINP1A_b.interval(BUTTON_INTERVAL);
    pinMode(P3FINP1A, OUTPUT);
}

void loop()
{
    // ------------------------------------------------
    // Read the pushbutton value into a variable
    // ------------------------------------------------
    P3DINP1A_b.update();
    P3DINP1B_b.update();
    P3DINP1C_b.update();
    P3DINP2A_b.update();
    P3DINP2B_b.update();
    P3DINP2C_b.update();
    P3DINP3A_b.update();
    P3DINP3B_b.update();
    P3DINP3C_b.update();
    //
    P3FINP1A_b.update();

    if (P3DINP1A_b.fell())
    {
        Serial.println("1016,1");
        P3DLED1A_state = 1;
    }
    else if (P3DINP1A_b.rose())
    {
        Serial.println("1016,0");
        P3DLED1A_state = 0;
    };
    if (P3DINP1B_b.fell())
    {
        Serial.println("1017,1");
        P3DLED1B_state = 1;
    }
    else if (P3DINP1B_b.rose())
    {
        Serial.println("1017,0");
        P3DLED1B_state = 0;
    };
    if (P3DINP1C_b.fell())
    {
        Serial.println("1018,1");
        P3DLED1C_state = 1;
    }
    else if (P3DINP1C_b.rose())
    {
        Serial.println("1018,0");
        P3DLED1C_state = 0;
    };
    if (P3DINP2A_b.fell())
    {
        Serial.println("1019,1");
        P3DLED2A_state = 1;
    }
    else if (P3DINP2A_b.rose())
    {
        Serial.println("1019,0");
        P3DLED2A_state = 0;
    };
    if (P3DINP2B_b.fell())
    {
        Serial.println("1020,1");
        P3DLED2B_state = 1;
    }
    else if (P3DINP2B_b.rose())
    {
        Serial.println("1020,0");
        P3DLED2B_state = 0;
    };
    if (P3DINP2C_b.fell())
    {
        Serial.println("1021,1");
        P3DLED2C_state = 1;
    }
    else if (P3DINP2C_b.rose())
    {
        Serial.println("1021,0");
        P3DLED2C_state = 0;
    };
    if (P3DINP3A_b.fell())
    {
        Serial.println("1022,1");
        P3DLED3A_state = 1;
    }
    else if (P3DINP3A_b.rose())
    {
        Serial.println("1022,0");
        P3DLED3A_state = 0;
    };
    if (P3DINP3B_b.fell())
    {
        Serial.println("1023,1");
        P3DLED3B_state = 1;
    }
    else if (P3DINP3B_b.rose())
    {
        Serial.println("1023,0");
        P3DLED3B_state = 0;
    };
    if (P3DINP3C_b.fell())
    {
        Serial.println("1024,1");
        P3DLED3C_state = 1;
    }
    else if (P3DINP3C_b.rose())
    {
        Serial.println("1024,0");
        P3DLED3C_state = 0;
    };
    //
    if (P3FINP1A_b.fell())
    {
        Serial.println("1033,1");
        P3FLED1A_state = 1;
    }
    else if (P3FINP1A_b.rose())
    {
        Serial.println("1033,0");
        P3FLED1A_state = 0;
    };


    // ------------------------------------------------
    // Writing to LEDS
    // ------------------------------------------------
    digitalWrite(P3DLED1A, 1);
    digitalWrite(P3DLED1B, 1);
    digitalWrite(P3DLED1C, 1);
    digitalWrite(P3DLED2A, 1);
    digitalWrite(P3DLED2B, 1);
    digitalWrite(P3DLED2C, 1);
    digitalWrite(P3DLED3A, 1);
    digitalWrite(P3DLED3B, 1);
    digitalWrite(P3DLED3C, 1);
    //
    digitalWrite(P3FLED1A, 1);
}
