from time import sleep
from multiprocessing import Process
import serial
# import collections
# from pygame import event
from pygame import KEYUP, KEYDOWN
# from pygame import K_ESCAPE, K_TAB, K_BACKSPACE, K_LEFT, K_RIGHT, K_UP, K_DOWN, K_PLUS, K_MINUS
# from pygame import K_RETURN, K_q, K_w
from constants import (
    K_DOCKING_1A, K_DOCKING_1B, K_DOCKING_1C, K_DOCKING_2A,
    K_DOCKING_2B, K_DOCKING_2C, K_DOCKING_3A, K_DOCKING_3B,
    K_DOCKING_3C, K_CRYOGENICS_1A, K_CRYOGENICS_1B, K_CRYOGENICS_2A,
    K_CRYOGENICS_2B, K_CRYOGENICS_3A, K_CRYOGENICS_3B, K_OPERATIONS_1A,
    K_OPERATIONS_1B, K_OPERATIONS_2A, K_OPERATIONS_2B, K_PYROTECHNICS_1A,
    K_PYROTECHNICS_1B, K_PYROTECHNICS_2A, K_PYROTECHNICS_2B, K_PYROTECHNICS_3A,
    K_PYROTECHNICS_3B, K_BOOSTER_1A, K_BOOSTER_1B, K_BOOSTER_1C, K_BOOSTER_2A,
    K_BOOSTER_2B, K_BOOSTER_2C, K_BOOSTER_3A, K_BOOSTER_3B, K_BOOSTER_3C,
    ##
    K_ABORT_1A, K_ABORT_1B, K_ABORT_2A, K_FUEL_1A,
    K_FUEL_1A_CLICK, K_FUEL_1B, K_FUEL_1B_CLICK,
    K_FUEL_2A, K_FUEL_2A_CLICK, K_FUEL_2B,
    K_FUEL_2B_CLICK, K_CONTROL_1A,
    K_CONTROL_1A_CLICK, K_CONTROL_1B,
    K_CONTROL_1B_CLICK, K_ENTER_2A,
    K_KEYPAD_1A, K_KEYPAD_1B, K_KEYPAD_1C,
    K_KEYPAD_2A, K_KEYPAD_2B, K_KEYPAD_2C,
    K_KEYPAD_3A, K_KEYPAD_3B, K_KEYPAD_3C,
    K_LIFE_SUPPORT_1A, K_LIFE_SUPPORT_1A_CLICK,
    K_LIFE_SUPPORT_1B, K_LIFE_SUPPORT_1B_CLICK,
    K_LIFE_SUPPORT_2A, K_LIFE_SUPPORT_2A_CLICK,
    K_LIFE_SUPPORT_2B, K_LIFE_SUPPORT_2B_CLICK,
    K_COMMUNICATION_1A
)
# from constants import (
#     L_A0, L_A1, L_A2, L_A3, L_A4, L_A5, L_A6, L_A7,
#     L_F0, L_F1, L_F2, L_F3, L_F4, L_F5, L_F6, L_F7,
#     L_DOCKING_1A, L_DOCKING_1B, L_DOCKING_1C,
#     L_DOCKING_2A, L_DOCKING_2B, L_DOCKING_2C,
#     L_DOCKING_3A, L_DOCKING_3B, L_DOCKING_3C,
#     L_BOOSTER_1A, L_BOOSTER_1B, L_BOOSTER_1C,
#     L_BOOSTER_2A, L_BOOSTER_2B, L_BOOSTER_2C,
#     L_BOOSTER_3A, L_BOOSTER_3B, L_BOOSTER_3C,
#     L_ENTER_2A,
#     L_KEYPAD_1A, L_KEYPAD_1B, L_KEYPAD_1C,
#     L_KEYPAD_2A, L_KEYPAD_2B, L_KEYPAD_2C,
#     L_KEYPAD_3A, L_KEYPAD_3B, L_KEYPAD_3C,
#     L_COMMUNICATION_1A
# )


class ControlPanel():

    def __init__(self):
        self.inputs = []
        self.leds = []

    def get_input(self, id=None, name=None):
        if id:
            for item in self.inputs:
                if item.id == id:
                    return item
        elif name:
            for item in self.inputs:
                if name.id == name:
                    return item
        else:
            return None

    def add(self, panel_item):
        if panel_item.type in ['button', 'switch', 'encoder']:
            self.inputs.append(panel_item)
        else:
            self.leds.append(panel_item)


class ArcadeButton():
    type = 'button'

    def __init__(self, name, id, event_down, event_up, led_id):
        self.name = name
        self.id = id
        self.value = 0
        self.event_down = event_down
        self.event_up = event_up
        self.led_id = led_id
        self.led_state = None


class ToggleSwitch():
    type = 'switch'

    def __init__(self, name, id, event_down, event_up):
        self.name = name
        self.id = id
        self.value = 0
        self.event_down = event_down
        self.event_up = event_up


class RotaryEncoder():
    type = 'encoder'

    def __init__(self, name, id, event_down, event_up):
        self.name = name
        self.id = id
        self.value = 0
        self.event_down = event_down
        self.event_up = event_up
        self.sound = None


CONTROL_PANEL = ControlPanel()
GAME_PANEL = ControlPanel()

# Define control panel buttons
# BUTTON = 1
# POTENTIOMETER = 2

# PanelItem = collections.namedtuple('PanelItem', ['name', 'type', 'id', 'event_down', 'event_up', 'value', 'LED', 'LED_state'])  # noqa:501

# Panel 3C
# BUTTON_FORCE_EXIT = PanelItem(name='Force Exit', type=BUTTON, id=1, event_down=event.Event(QUIT, key=K_ESCAPE), event_up=event.Event(QUIT, key=K_ESCAPE), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_MENU  = PanelItem(name='Menu', type=BUTTON, id=2, event_down=event.Event(KEYDOWN, key=K_TAB), event_up=event.Event(KEYUP, key=K_TAB), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_BACKSPACE = PanelItem(name='Exit', type=BUTTON, id=3, event_down=event.Event(KEYDOWN, key=K_BACKSPACE), event_up=event.Event(KEYUP, key=K_BACKSPACE), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_LEFT = PanelItem(name='Left', type=BUTTON, id=4, event_down=event.Event(KEYDOWN, key=K_LEFT), event_up=event.Event(KEYUP, key=K_LEFT), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_RIGHT = PanelItem(name='Right', type=BUTTON, id=5, event_down=event.Event(KEYDOWN, key=K_RIGHT), event_up=event.Event(KEYUP, key=K_RIGHT), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_UP = PanelItem(name='Up', type=BUTTON, id=6, event_down=event.Event(KEYDOWN, key=K_UP), event_up=event.Event(KEYUP, key=K_UP), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_DOWN = PanelItem(name='Down', type=BUTTON, id=7, event_down=event.Event(KEYDOWN, key=K_DOWN), event_up=event.Event(KEYUP, key=K_DOWN), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_PLUS = PanelItem(name='Plus', type=BUTTON, id=8, event_down=event.Event(KEYDOWN, key=K_PLUS), event_up=event.Event(KEYUP, key=K_PLUS), value=None, LED=None, LED_state=None)  # noqa:501
# BUTTON_MINUS = PanelItem(name='Minus', type=BUTTON, id=9, event_down=event.Event(KEYDOWN, key=K_MINUS), event_up=event.Event(KEYUP, key=K_MINUS), value=None, LED=None, LED_state=None)  # noqa:501
# Panel 4C
# BUTTON_ENTER = PanelItem(name='Enter', type=BUTTON, id=10, event_down=event.Event(KEYDOWN, key=K_RETURN), event_up=event.Event(KEYUP, key=K_RETURN), value=None, LED=None, LED_state=None)  # noqa:501
# POT_1 = PanelItem(name='Power', type=POTENTIOMETER, id=21, event_down=event.Event(KEYDOWN, key=K_q), event_up=event.Event(KEYUP, key=K_q), value=50, LED=None, LED_state=None)  # noqa:501
# POT_2 = PanelItem(name='Angle', type=POTENTIOMETER, id=22, event_down=event.Event(KEYDOWN, key=K_w), event_up=event.Event(KEYUP, key=K_w), value=45, LED=None, LED_state=None)  # noqa:501
# POT_3 = PanelItem(name='Other3', type=POTENTIOMETER, id=23, event_down=event.Event(KEYDOWN, key=K_RETURN), event_up=event.Event(KEYUP, key=K_RETURN), value=None, LED=None, LED_state=None)  # noqa:501
# POT_4 = PanelItem(name='Other4', type=POTENTIOMETER, id=24, event_down=event.Event(KEYDOWN, key=K_RETURN), event_up=event.Event(KEYUP, key=K_RETURN), value=None, LED=None, LED_state=None)  # noqa:501


#########################################################
# Define buttons
#########################################################
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_ENTER_2A', id=K_ENTER_2A, event_down=[KEYDOWN, K_ENTER_2A], event_up=[KEYUP, K_ENTER_2A], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_1C', id=K_KEYPAD_1A, event_down=[KEYDOWN, K_KEYPAD_1A], event_up=[KEYUP, K_KEYPAD_1A], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_1B', id=K_KEYPAD_1B, event_down=[KEYDOWN, K_KEYPAD_1B], event_up=[KEYUP, K_KEYPAD_1B], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_1A', id=K_KEYPAD_1C, event_down=[KEYDOWN, K_KEYPAD_1C], event_up=[KEYUP, K_KEYPAD_1C], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_2C', id=K_KEYPAD_2A, event_down=[KEYDOWN, K_KEYPAD_2A], event_up=[KEYUP, K_KEYPAD_2A], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_2B', id=K_KEYPAD_2B, event_down=[KEYDOWN, K_KEYPAD_2B], event_up=[KEYUP, K_KEYPAD_2B], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_2A', id=K_KEYPAD_2C, event_down=[KEYDOWN, K_KEYPAD_2C], event_up=[KEYUP, K_KEYPAD_2C], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_3C', id=K_KEYPAD_3A, event_down=[KEYDOWN, K_KEYPAD_3A], event_up=[KEYUP, K_KEYPAD_3A], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_3B', id=K_KEYPAD_3B, event_down=[KEYDOWN, K_KEYPAD_3B], event_up=[KEYUP, K_KEYPAD_3B], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_KEYPAD_3A', id=K_KEYPAD_3C, event_down=[KEYDOWN, K_KEYPAD_3C], event_up=[KEYUP, K_KEYPAD_3C], led_id=None))  # noqa:501
CONTROL_PANEL.add(ArcadeButton(name='BUTTON_COMMUNICATION_1A', id=K_COMMUNICATION_1A, event_down=[KEYDOWN, K_COMMUNICATION_1A], event_up=[KEYUP, K_COMMUNICATION_1A], led_id=None))  # noqa:501
CONTROL_PANEL.add(ToggleSwitch(name='ABORT 1B', id=K_ABORT_1A, event_down=[KEYDOWN, K_ABORT_1A], event_up=[KEYUP, K_ABORT_1A]))  # noqa:501
CONTROL_PANEL.add(ToggleSwitch(name='ABORT 1A', id=K_ABORT_1B, event_down=[KEYDOWN, K_ABORT_1B], event_up=[KEYUP, K_ABORT_1B]))  # noqa:501
CONTROL_PANEL.add(ToggleSwitch(name='ABORT 2A', id=K_ABORT_2A, event_down=[KEYDOWN, K_ABORT_2A], event_up=[KEYUP, K_ABORT_2A]))  # noqa:501
CONTROL_PANEL.add(RotaryEncoder(name='ANGLE', id=K_CONTROL_1A, event_down=[KEYDOWN, K_CONTROL_1A], event_up=[KEYUP, K_CONTROL_1A]))  # noqa:501
CONTROL_PANEL.add(RotaryEncoder(name='POWER', id=K_CONTROL_1B, event_down=[KEYDOWN, K_CONTROL_1B], event_up=[KEYUP, K_CONTROL_1B]))  # noqa:501
##
GAME_PANEL.add(ArcadeButton(name='DOCKING 3', id=K_DOCKING_1A, event_down=[KEYDOWN, K_DOCKING_1A], event_up=[KEYUP, K_DOCKING_1A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 2', id=K_DOCKING_1B, event_down=[KEYDOWN, K_DOCKING_1B], event_up=[KEYUP, K_DOCKING_1B], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 1', id=K_DOCKING_1C, event_down=[KEYDOWN, K_DOCKING_1C], event_up=[KEYUP, K_DOCKING_1C], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 6', id=K_DOCKING_2A, event_down=[KEYDOWN, K_DOCKING_2A], event_up=[KEYUP, K_DOCKING_2A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 5', id=K_DOCKING_2B, event_down=[KEYDOWN, K_DOCKING_2B], event_up=[KEYUP, K_DOCKING_2B], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 4', id=K_DOCKING_2C, event_down=[KEYDOWN, K_DOCKING_2C], event_up=[KEYUP, K_DOCKING_2C], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 9', id=K_DOCKING_3A, event_down=[KEYDOWN, K_DOCKING_3A], event_up=[KEYUP, K_DOCKING_3A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 8', id=K_DOCKING_3B, event_down=[KEYDOWN, K_DOCKING_3B], event_up=[KEYUP, K_DOCKING_3B], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='DOCKING 7', id=K_DOCKING_3C, event_down=[KEYDOWN, K_DOCKING_3C], event_up=[KEYUP, K_DOCKING_3C], led_id=None))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='CRYOGENICS B', id=K_CRYOGENICS_1A, event_down=[KEYDOWN, K_CRYOGENICS_1A], event_up=[KEYUP, K_CRYOGENICS_1A]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='CRYOGENICS A', id=K_CRYOGENICS_1B, event_down=[KEYDOWN, K_CRYOGENICS_1B], event_up=[KEYUP, K_CRYOGENICS_1B]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='CRYOGENICS D', id=K_CRYOGENICS_2A, event_down=[KEYDOWN, K_CRYOGENICS_2A], event_up=[KEYUP, K_CRYOGENICS_2A]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='CRYOGENICS C', id=K_CRYOGENICS_2B, event_down=[KEYDOWN, K_CRYOGENICS_2B], event_up=[KEYUP, K_CRYOGENICS_2B]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='CRYOGENICS F', id=K_CRYOGENICS_3A, event_down=[KEYDOWN, K_CRYOGENICS_3A], event_up=[KEYUP, K_CRYOGENICS_3A]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='CRYOGENICS E', id=K_CRYOGENICS_3B, event_down=[KEYDOWN, K_CRYOGENICS_3B], event_up=[KEYUP, K_CRYOGENICS_3B]))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='OPERATIONS B', id=K_OPERATIONS_1A, event_down=[KEYDOWN, K_OPERATIONS_1A], event_up=[KEYUP, K_OPERATIONS_1A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='OPERATIONS A', id=K_OPERATIONS_1B, event_down=[KEYDOWN, K_OPERATIONS_1B], event_up=[KEYUP, K_OPERATIONS_1B], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='OPERATIONS D', id=K_OPERATIONS_2A, event_down=[KEYDOWN, K_OPERATIONS_2A], event_up=[KEYUP, K_OPERATIONS_2A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='OPERATIONS C', id=K_OPERATIONS_2B, event_down=[KEYDOWN, K_OPERATIONS_2B], event_up=[KEYUP, K_OPERATIONS_2B], led_id=None))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='PYROTECHNICS B', id=K_PYROTECHNICS_1A, event_down=[KEYDOWN, K_PYROTECHNICS_1A], event_up=[KEYUP, K_PYROTECHNICS_1A]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='PYROTECHNICS A', id=K_PYROTECHNICS_1B, event_down=[KEYDOWN, K_PYROTECHNICS_1B], event_up=[KEYUP, K_PYROTECHNICS_1B]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='PYROTECHNICS D', id=K_PYROTECHNICS_2A, event_down=[KEYDOWN, K_PYROTECHNICS_2A], event_up=[KEYUP, K_PYROTECHNICS_2A]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='PYROTECHNICS C', id=K_PYROTECHNICS_2B, event_down=[KEYDOWN, K_PYROTECHNICS_2B], event_up=[KEYUP, K_PYROTECHNICS_2B]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='PYROTECHNICS F', id=K_PYROTECHNICS_3A, event_down=[KEYDOWN, K_PYROTECHNICS_3A], event_up=[KEYUP, K_PYROTECHNICS_3A]))  # noqa:501
GAME_PANEL.add(ToggleSwitch(name='PYROTECHNICS E', id=K_PYROTECHNICS_3B, event_down=[KEYDOWN, K_PYROTECHNICS_3B], event_up=[KEYUP, K_PYROTECHNICS_3B]))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 3', id=K_BOOSTER_1A, event_down=[KEYDOWN, K_BOOSTER_1A], event_up=[KEYUP, K_BOOSTER_1A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 2', id=K_BOOSTER_1B, event_down=[KEYDOWN, K_BOOSTER_1B], event_up=[KEYUP, K_BOOSTER_1B], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 1', id=K_BOOSTER_1C, event_down=[KEYDOWN, K_BOOSTER_1C], event_up=[KEYUP, K_BOOSTER_1C], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 6', id=K_BOOSTER_2A, event_down=[KEYDOWN, K_BOOSTER_2A], event_up=[KEYUP, K_BOOSTER_2A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 5', id=K_BOOSTER_2B, event_down=[KEYDOWN, K_BOOSTER_2B], event_up=[KEYUP, K_BOOSTER_2B], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 4', id=K_BOOSTER_2C, event_down=[KEYDOWN, K_BOOSTER_2C], event_up=[KEYUP, K_BOOSTER_2C], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 9', id=K_BOOSTER_3A, event_down=[KEYDOWN, K_BOOSTER_3A], event_up=[KEYUP, K_BOOSTER_3A], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 8', id=K_BOOSTER_3B, event_down=[KEYDOWN, K_BOOSTER_3B], event_up=[KEYUP, K_BOOSTER_3B], led_id=None))  # noqa:501
GAME_PANEL.add(ArcadeButton(name='BOOSTER 7', id=K_BOOSTER_3C, event_down=[KEYDOWN, K_BOOSTER_3C], event_up=[KEYUP, K_BOOSTER_3C], led_id=None))  # noqa:501
##
GAME_PANEL.add(RotaryEncoder(name='FUEL BOOST', id=K_FUEL_1B, event_down=[KEYUP, K_FUEL_1B], event_up=[KEYDOWN, K_FUEL_1B]))  # noqa:501
GAME_PANEL.add(RotaryEncoder(name='EFFICIENCY MODE', id=K_FUEL_1A, event_down=[KEYUP, K_FUEL_1A], event_up=[KEYDOWN, K_FUEL_1A]))  # noqa:501
GAME_PANEL.add(RotaryEncoder(name='AUTO', id=K_FUEL_2B, event_down=[KEYUP, K_FUEL_2B], event_up=[KEYDOWN, K_FUEL_2B]))  # noqa:501
GAME_PANEL.add(RotaryEncoder(name='CONSERVE PWR', id=K_FUEL_2A, event_down=[KEYUP, K_FUEL_2A], event_up=[KEYDOWN, K_FUEL_2A]))  # noqa:501

GAME_PANEL.add(RotaryEncoder(name='OXYGEN', id=K_LIFE_SUPPORT_1A, event_down=[KEYUP, K_LIFE_SUPPORT_1A], event_up=[KEYDOWN, K_LIFE_SUPPORT_1A]))  # noqa:501
GAME_PANEL.add(RotaryEncoder(name='AIR FILTER', id=K_LIFE_SUPPORT_1B, event_down=[KEYUP, K_LIFE_SUPPORT_1B], event_up=[KEYDOWN, K_LIFE_SUPPORT_1B]))  # noqa:501
GAME_PANEL.add(RotaryEncoder(name='VENTIlATION', id=K_LIFE_SUPPORT_2A, event_down=[KEYUP, K_LIFE_SUPPORT_2A], event_up=[KEYDOWN, K_LIFE_SUPPORT_2A]))  # noqa:501
GAME_PANEL.add(RotaryEncoder(name='WATER FILTER', id=K_LIFE_SUPPORT_2B, event_down=[KEYUP, K_LIFE_SUPPORT_2B], event_up=[KEYDOWN, K_LIFE_SUPPORT_2B]))  # noqa:501


class ControlPanelProcess(Process):

    def __init__(self, queue):
        Process.__init__(self, name="panel control")
        self.running = True
        self.queue = queue
        self.all_inputs = CONTROL_PANEL.inputs + GAME_PANEL.inputs

    def run(self):
        try:
            serialFromArduino1 = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
            serialFromArduino2 = serial.Serial('/dev/ttyACM1', 9600, timeout=1)
            serialFromArduino3 = serial.Serial('/dev/ttyACM2', 9600, timeout=1)
            serialFromArduino4 = serial.Serial('/dev/ttyACM3', 9600, timeout=1)
        except serial.serialutil.SerialException:
            return

        # while serialFromArduino1.isOpen() and serialFromArduino2.isOpen() and serialFromArduino3.isOpen() and serialFromArduino4.isOpen():  # noqa:501
        while self.running:

            if not serialFromArduino1.isOpen():
                try:
                    serialFromArduino1 = serial.Serial('/dev/ttyACM0', 9600, timeout=1)  # noqa:501
                except serial.serialutil.SerialException:
                    pass
            if not serialFromArduino2.isOpen():
                try:
                    serialFromArduino3 = serial.Serial('/dev/ttyACM2', 9600, timeout=1)  # noqa:501
                except serial.serialutil.SerialException:
                    pass
            if not serialFromArduino3.isOpen():
                try:
                    serialFromArduino3 = serial.Serial('/dev/ttyACM2', 9600, timeout=1)  # noqa:501
                except serial.serialutil.SerialException:
                    pass
            if not serialFromArduino4.isOpen():
                try:
                    serialFromArduino4 = serial.Serial('/dev/ttyACM3', 9600, timeout=1)  # noqa:501
                except serial.serialutil.SerialException:
                    pass

            line = None
            # Read a '\n' terminated line from serial
            try:
                if serialFromArduino1.inWaiting() > 0:
                    line = serialFromArduino1.readline().rstrip().decode()
                elif serialFromArduino2.inWaiting() > 0:
                    line = serialFromArduino2.readline().rstrip().decode()
                elif serialFromArduino3.inWaiting() > 0:
                    line = serialFromArduino3.readline().rstrip().decode()
                elif serialFromArduino4.inWaiting() > 0:
                    line = serialFromArduino4.readline().rstrip().decode()
            except serial.serialutil.SerialException:
                continue

            if line:
                try:
                    serial_event = self.process_input_from_control_panel(line)  # noqa:501
                except TypeError:
                    continue

                if serial_event:
                    self.queue.put(serial_event)

            sleep(0.01)

    def process_input_from_control_panel(self, input_string):
        ''' Process control panel input from arduino '''

        input_array = input_string.split(',')
        try:
            input_array = [int(input_array[0]), int(input_array[1])]
        except ValueError:
            print('Failed to process input. Received:', input_array)
            return None
        except IndexError:
            return None

        # all_inputs = CONTROL_PANEL.inputs + GAME_PANEL.inputs

        for input_item in self.all_inputs:
            if input_array[0] == input_item.id:
                if input_array[1] == 0:
                    return input_item.event_up
                else:
                    return input_item.event_down
