#!/usr/bin/python3

import os
from multiprocessing import Queue
import pygame
from subprocess import call
# from time import sleep
from constants import (
    PHYSICAL_SCREEN, CLOCK_TICK_RATE,
    WHITE
)
from constants import (
    ALIGN_LEFT, ALIGN_CENTER_H, ALIGN_RIGHT,
    ALIGN_TOP, ALIGN_CENTER_V, ALIGN_BOTTOM
)

import control_panel
from games.projectilegravity.game import ProjectileGravity
from games.mission_control.game import MissionControl
# from games.solar_system.game import SolarSystem
# from games.game4.game import Game4
# from games.game5.game import Game5
# from games.game6.game import Game6

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class MenuButton(pygame.sprite.DirtySprite):

    ''' A game menu button '''

    def __init__(self, position, image, selected_image, game_number):
        """
        Animated sprite object.

        Args:
            position: x, y coordinate on the screen
                to place the AnimatedSprite.
            images: Images to use in the animation.
        """
        super(MenuButton, self).__init__()

        self.dirty = 1
        self.position = position
        self.size = (420, 420)  # This should match the size of the images.
        self.number = game_number
        self.rect = pygame.Rect(self.position, self.size)
        self.unselected_image = image
        self.selected_image = selected_image
        self.image = self.unselected_image

    def set_dirty(self):
        self.dirty = 1

    def update(self, selected_game):
        """ Updates image"""
        if selected_game == self.number:
            self.image = self.selected_image
        else:
            self.image = self.unselected_image

        self.set_dirty()


class Text(pygame.sprite.DirtySprite):
    ''' Master Text Sprite '''

    def __init__(self, pos, color, typeface, text, align_h=ALIGN_LEFT, align_v=ALIGN_TOP):  # noqa:501

        super(Text, self).__init__()

        self.color = color
        self.typeface = typeface
        self.text = typeface.render(str(text), 1, self.color)
        self.align_h = align_h
        self.align_v = align_v
        self.master_position = pos
        self.position = self.get_position()
        self.dirty = 1
        self.image = self.text
        self.rect = pygame.Rect(
            self.position,
            (self.text.get_rect().width, self.text.get_rect().height))

    def set_dirty(self):
        ''' Set text to update '''
        self.dirty = 1

    def get_position(self):
        ''' get new position of text '''
        x = 0
        y = 0
        if self.align_h == ALIGN_LEFT:
            x = self.master_position[0]
        elif self.align_h == ALIGN_CENTER_H:
            x = self.master_position[0] - self.text.get_rect().width / 2
        elif self.align_h == ALIGN_RIGHT:
            x = self.master_position[0] - self.text.get_rect().width

        if self.align_v == ALIGN_TOP:
            y = self.master_position[1]
        elif self.align_v == ALIGN_CENTER_V:
            y = self.master_position[1] - self.text.get_rect().height / 2
        elif self.align_v == ALIGN_BOTTOM:
            y = self.master_position[1] - self.text.get_rect().height

        return (x, y)

    def update(self, text):
        ''' Update text '''
        self.text = self.typeface.render(str(text), 1, self.color)
        self.image = self.text
        self.position = self.get_position()
        self.rect = pygame.Rect(
            self.position,
            (self.text.get_rect().width, self.text.get_rect().height))
        self.set_dirty()


class Game():

    change_to_windowed_code = [1059, 1059, 1059, 1059, 1059, 1059]
    shutdown_code = [1061, 1061, 1061, 1061, 1061, 1061]

    def __init__(self):
        self.shutdown_attempt = []
        self.change_to_windowed_attempt = []
        self.running = True
        self.game = None
        self.selected_game = 0

        self.all_sprites = pygame.sprite.LayeredDirty()
        self.all_buttons = pygame.sprite.Group()

        self.queue = Queue()

        self.games = []

    def exit_or_shutdown(self, shutdown=False):
        self.running = False
        self.queue.close()
        self.control_panel_process.terminate()
        pygame.display.quit()
        pygame.quit()
        print('Terminated')
        if shutdown:
            call("sudo halt", shell=True)

    def change_to_windowed(self):
        pygame.mouse.set_visible(True)
        self.screen = pygame.display.set_mode([1920, 1080])

    def start_pygame(self):
        pygame.mixer.pre_init(44100, -16, 3, 512)
        pygame.mixer.init()
        pygame.init()
        pygame.mouse.set_visible(False)

        # Sets screen to fullscreen with no frame/controls
        # ESC to exit full-screen
        self.screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN | pygame.NOFRAME)  # noqa:501
        # self.screen = pygame.display.set_mode([1920, 1080])

        self.clock = pygame.time.Clock()

    def setup_game_menu(self):

        self.title_font = pygame.font.SysFont("Minecraftia", 72)
        self.background = pygame.image.load(os.path.join(BASE_DIR, 'src/games/resources/main_menu.png')).convert_alpha()  # noqa:501

        self.games = [
            ProjectileGravity,  # noqa:501
            MissionControl,  # noqa:501
            # SolarSystem(control_panel_queue=ARDUINO_1_QUEUE, screen=SCREEN),  # noqa:501
            # Game4(control_panel_queue=ARDUINO_1_QUEUE, screen=SCREEN),  # noqa:501
            # Game5(control_panel_queue=ARDUINO_1_QUEUE, screen=SCREEN),  # noqa:501
            # Game6(control_panel_queue=ARDUINO_1_QUEUE, screen=SCREEN)  # noqa:501
        ]

        for i, game in enumerate(self.games):

            unselected = pygame.image.load(os.path.join(BASE_DIR, 'src/games/' + game.directory + '/resources/images/menubutton/unselected.png')).convert_alpha()  # noqa:501
            selected = pygame.image.load(os.path.join(BASE_DIR, 'src/games/' + game.directory + '/resources/images/menubutton/selected.png')).convert_alpha()  # noqa:501
            if i == 0:
                coords = (160, 40)
            elif i == 1:
                coords = (750, 40)
            elif i == 2:
                coords = (1340, 40)
            elif i == 3:
                coords = (160, 580)
            elif i == 4:
                coords = (750, 580)
            elif i == 5:
                coords = (1340, 580)

            button_to_create = MenuButton(position=coords, image=unselected, selected_image=selected, game_number=i)  # noqa:501
            self.all_buttons.add(button_to_create)
        try:
            self.game_title = Text((PHYSICAL_SCREEN.width / 2, PHYSICAL_SCREEN.height / 2), WHITE, self.title_font, self.games[0].name, align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)  # noqa:501
        except IndexError:
            self.game_title = Text((PHYSICAL_SCREEN.width / 2, PHYSICAL_SCREEN.height / 2), WHITE, self.title_font, '', align_h=ALIGN_CENTER_H, align_v=ALIGN_CENTER_V)  # noqa:501

        self.all_sprites.add(self.all_buttons, layer=1)
        self.all_sprites.add(self.game_title, layer=2)
        self.all_sprites.clear(self.screen, self.background)

    def game_loop(self):
        ''' Thread for the game '''

        self.start_pygame()
        self.setup_game_menu()

        default_menu_button_sound = pygame.mixer.Sound(os.path.join(BASE_DIR, 'src/games/mission_control/resources/sounds/buttons/Tiny_Button_Push.wav'))  # noqa:501

        self.control_panel_process = control_panel.ControlPanelProcess(self.queue)  # noqa:501
        self.control_panel_process.start()

        while self.running:

            while not self.queue.empty():
                new_event = self.queue.get()
                try:
                    pygame.event.post(pygame.event.Event(new_event[0], key=new_event[1]))  # noqa:501
                except IndexError:
                    print(new_event)

            # Input handling
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    self.exit_or_shutdown()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:  # noqa:501
                    self.exit_or_shutdown()

                if event.type == pygame.KEYDOWN:

                    default_menu_button_sound.play()

                    if event.key == pygame.K_RIGHT or event.key == control_panel.K_KEYPAD_2A:  # noqa:501
                        if self.selected_game + 1 == len(self.games):
                            self.selected_game = 0
                            self.all_sprites.sprites()[self.selected_game].set_dirty()  # noqa:501
                            self.all_sprites.sprites()[len(self.games) - 1].set_dirty()  # noqa:501
                        else:
                            self.selected_game += 1
                            self.all_sprites.sprites()[self.selected_game].set_dirty()  # noqa:501
                            self.all_sprites.sprites()[self.selected_game - 1].set_dirty()  # noqa:501

                    elif event.key == pygame.K_LEFT or event.key == control_panel.K_KEYPAD_2C:  # noqa:501
                        if self.selected_game - 1 < 0:
                            self.selected_game = len(self.games) - 1
                            self.all_sprites.sprites()[self.selected_game].set_dirty()  # noqa:501
                            self.all_sprites.sprites()[0].set_dirty()  # noqa:501
                        else:
                            self.selected_game -= 1
                            self.all_sprites.sprites()[self.selected_game].set_dirty()  # noqa:501
                            self.all_sprites.sprites()[self.selected_game + 1].set_dirty()  # noqa:501

                    # Choose a game
                    elif event.key == pygame.K_RETURN or event.key == control_panel.K_ENTER_2A:  # noqa:501
                        # Start selected game
                        print('Start Game', self.selected_game)
                        # game_state = GAME_STATE_RUNNING
                        self.game_title.update('Loading game...')
                        dirty_rects = self.all_sprites.draw(self.screen)
                        pygame.display.update(dirty_rects)

                        game_class = self.games[self.selected_game]
                        self.game = game_class(control_panel_queue=self.queue, screen=self.screen)  # noqa:501
                        self.game.start()
                        self.game_title.update(self.games[self.selected_game].name)  # noqa:501
                        self.game = None

                        self.screen.blit(self.background, (0, 0))
                        for button in self.all_sprites.sprites():
                            button.set_dirty()

                        pygame.display.flip()

                        # game_state = None

                    # Reset shutdown attempt
                    elif event.key == control_panel.K_KEYPAD_2B or event.key == pygame.K_r:  # noqa:501
                        self.shutdown_attempt = []
                        self.change_to_windowed_attempt = []
                        print('reset codes')

                    elif event.key == control_panel.K_BOOSTER_1C or event.key == pygame.K_q:  # noqa:501
                        self.shutdown_attempt.append(control_panel.K_BOOSTER_1C)  # noqa:501

                    elif event.key == control_panel.K_BOOSTER_1A or event.key == pygame.K_w:  # noqa:501
                        self.change_to_windowed_attempt.append(control_panel.K_BOOSTER_1A)  # noqa:501

                    else:
                        self.shutdown_attempt = []
                        self.change_to_windowed_attempt = []

                    try:
                        self.game_title.update(self.games[self.selected_game].name)  # noqa:501
                    except IndexError:
                        pass

            if self.shutdown_attempt == self.shutdown_code:
                self.exit_or_shutdown(shutdown=True)

            if self.change_to_windowed_attempt == self.change_to_windowed_code:  # noqa:501
                self.change_to_windowed()

            # Draw BACKGROUND
            # SCREEN.blit(BACKGROUND, (0, 0))
            self.all_buttons.update(self.selected_game)
            dirty_rects = self.all_sprites.draw(self.screen)
            pygame.display.update(dirty_rects)

            # pygame.display.flip()
            self.clock.tick(CLOCK_TICK_RATE)
            # pygame.display.set_caption("fps: " + str(CLOCK.get_fps()))
            pygame.display.set_caption("Lewisburg Children's Museum")


if __name__ == '__main__':

    GAME = Game()
    GAME.game_loop()
    print("Game stopped")
