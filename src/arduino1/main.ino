#include <Bounce2.h>

#include "Rotary.h"

#include "include/constants.h"

#define BUTTON_INTERVAL 25

// Define rotary encoders
Rotary r1 = Rotary(P3BINP1A1, P3BINP1A3);
Rotary r2 = Rotary(P3BINP1B1, P3BINP1B3);
Rotary r3 = Rotary(P3BINP2A1, P3BINP2A3);

// Define toggle switches and buttons
Bounce P3AINP1A_b = Bounce();
Bounce P3AINP1B_b = Bounce();
Bounce P3AINP2A_b = Bounce();
//
Bounce P3CINP2A_b = Bounce();

void check1()
{
    unsigned char result = r1.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1003,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1003,0");
    }
}

void check2()
{
    unsigned char result = r2.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1005,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1005,0");
    }
}

void check3()
{
    unsigned char result = r3.process();
    if (result == DIR_NONE)
    {
        // do nothing
    }
    else if (result == DIR_CW)
    {
        Serial.println("1007,1");
    }
    else if (result == DIR_CCW)
    {
        Serial.println("1007,0");
    }
}

void setup()
{
    Serial.begin(9600);

    // Attach interrupts to rotary encoders
    attachInterrupt(digitalPinToInterrupt(P3BINP1A1), check1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3BINP1A3), check1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3BINP1B1), check2, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3BINP1B3), check2, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3BINP2A1), check3, CHANGE);
    attachInterrupt(digitalPinToInterrupt(P3BINP2A3), check3, CHANGE);
    // Digital pins 23-53
    // pinMode(P1ALEDS, OUTPUT);
    P3AINP1A_b.attach(P3AINP1A, INPUT_PULLUP);
    P3AINP1A_b.interval(BUTTON_INTERVAL);
    P3AINP1B_b.attach(P3AINP1B, INPUT_PULLUP);
    P3AINP1B_b.interval(BUTTON_INTERVAL);
    P3AINP2A_b.attach(P3AINP2A, INPUT_PULLUP);
    P3AINP2A_b.interval(BUTTON_INTERVAL);
    //
    P3CINP2A_b.attach(P3CINP2A, INPUT_PULLUP);
    P3CINP2A_b.interval(BUTTON_INTERVAL);
    pinMode(P3CLED2A, OUTPUT);

}

void loop()
{
    // ------------------------------------------------
    // Read the pushbutton value into a variable
    // ------------------------------------------------
    // ROW 3
    P3AINP1A_b.update();
    P3AINP1B_b.update();
    P3AINP2A_b.update();
    P3CINP2A_b.update();

    // ------------------------------------------------
    // Check for changes
    // ------------------------------------------------
    // ROW 3
    if (P3AINP1A_b.fell())
    {
        Serial.println("1000,0");
    }
    else if (P3AINP1A_b.rose())
    {
        Serial.println("1000,1"); 
    };
    if (P3AINP1B_b.fell())
    {
        Serial.println("1001,0");
    }
    else if (P3AINP1B_b.rose())
    {
        Serial.println("1001,1");
    };
    if (P3AINP2A_b.fell())
    {
        Serial.println("1002,0");
    }
    else if (P3AINP2A_b.rose())
    {
        Serial.println("1002,1");
    };


    if (P3CINP2A_b.fell())
    {
        Serial.println("1015,0");
        P3CLED2A_state = 0;
    }
    else if (P3CINP2A_b.rose())
    {
        Serial.println("1015,1");
        P3CLED2A_state = 1;
    };

    // ------------------------------------------------
    // Writing to LEDS
    // ------------------------------------------------
    // ROW 3
    digitalWrite(P3CLED2A, 1);

}