
//----------------------------
// PANEL ROW 3
// Define pin numbers
//----------------------------
const int P3AINP1A = 22;
const int P3AINP1B = 23;
const int P3AINP2A = 24;
//
const int P3BINP1A1 = 2;
const int P3BINP1A3 = 3;
const int P3BINP1B1 = 18;
const int P3BINP1B3 = 19;
const int P3BINP2A1 = 20;
const int P3BINP2A3 = 21;
//
const int P3CINP2A = 25;
const int P3CLED2A = 26;
//

//----------------------------
// PANEL ROW 3
// Storage for LED states
//----------------------------
int P3CLED2A_state = 0;
